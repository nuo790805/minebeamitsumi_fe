let mixin = {
  methods: {
    setlineMode: function (bool) {
      return this.$store.dispatch('lineMode', bool)
    }
  },
  computed: {
    curLan: function () {
      return this.State.lan
    },
    Button: function () {
      let btns = {}
      if (this.curLan === 'EN') {
        btns.standard = require('./assets/others/btnDash_en.png')
        btns.counter_1 = require('./assets/others/btnDash_blue_en1.png')
        btns.counter_2 = require('./assets/others/btnDash_blue_en2.png')
      } else if (this.curLan === 'CN') {
        btns.standard = require('./assets/others/btnDash_ch.png')
        btns.counter_1 = require('./assets/others/btnDash_blue_ch1.png')
        btns.counter_2 = require('./assets/others/btnDash_blue_ch2.png')
      } else if (this.curLan === 'JP') {
        btns.standard = require('./assets/others/btnDash_jp.png')
        btns.counter_1 = require('./assets/others/btnDash_blue_jp1.png')
        btns.counter_2 = require('./assets/others/btnDash_blue_jp2.png')
      }
      return btns
    },
    State: function () {
      return this.$store.state
    },
    VideoSrc: function () {
      let source = null
      try {
        source = require('./assets/video/' + this.State.video + '.mp4')
      } catch (e) {
        source = require('./assets/video/default.mp4')
      }
      return source
    }
  }
}
export default mixin
