import Vue from "vue";
import Vuex from "vuex";
import { _Common, Common } from "@/translate/common.js";
// import { _unsort, unsort } from '@/translate/unsort.js'
import { _APPs, APPs } from "@/translate/application.js";
import { Page } from "@/translate/page.js";
import { _Seal, Seal } from "@/translate/seal.js";
import { _Grease, Grease } from "@/translate/grease.js";
import { _Retainer, Retainer } from "@/translate/retainer.js";
import { _Inter, Inter } from "@/translate/interal.js";
import { _Special, Special } from "@/translate/special.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    _APPs,
    APPs,
    _Common,
    Common,
    // _unsort,
    // unsort,
    _Seal,
    Seal,

    _Grease,
    Grease,
    _Retainer,
    Retainer,
    _Inter,
    Inter,
    _Special,
    Special,
    Page,
    currentPage: "",
    lan: "JP",
    serial: [],
    folderNumber: 0,
    lineMode: true,
    video: "",
    video_enable: false
  },
  mutations: {
    currentPage(state, str) {
      state.currentPage = str;
    },
    changeLanguage(state, lan) {
      state.lan = lan;
      state.Common = state._Common[lan];
      state.Seal = state._Seal[lan];
      state.Grease = state._Grease[lan];
      state.Retainer = state._Retainer[lan];
      state.Special = state._Special[lan];
      state.Inter = state._Inter[lan];
      state.APPs = state._APPs[lan];
      // state.Page.Turbo = state.Page._Turbo[lan]
      for (const str in state.Page) {
        if (str.indexOf("_") !== 0) {
          state.Page[str] = state.Page[`_${str}`][lan];
        }
      }
    },
    serialsContext(state, arr) {
      state.serial = arr;
    },
    folderNumber(state, num) {
      state.folderNumber = num;
    },
    lineMode(state, bool) {
      state.lineMode = bool;
    },
    videoSrc(state, str) {
      state.video = str;
    },
    videoEnable(state, bool) {
      state.video_enable = bool;
    }
  },
  actions: {
    currentPage(context, str) {
      context.commit("currentPage", str);
    },
    changeLanguage(context, lan) {
      context.commit("changeLanguage", lan);
    },
    serialsContext(context, arr) {
      context.commit("serialsContext", arr);
    },
    folderNumber(context, num) {
      context.commit("folderNumber", num);
    },
    lineMode(context, bool) {
      context.commit("lineMode", bool);
    },
    videoSrc(context, str) {
      // console.log('str', str)
      context.commit("videoSrc", str);
    },
    videoEnable(context, bool) {
      context.commit("videoEnable", bool);
    }
  }
});
