import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Product from './views/Product.vue'
import Turbo from './components/products/Turbo.vue'
import EpsMotor from './components/products/EpsMotor.vue'
import ABS_ESC from './components/products/ABS_ESC.vue'
import EtcEgrValue from './components/products/EtcEgrValue.vue'
import WiperMotor from './components/products/WiperMotor.vue'

import HVAC from './components/products/HVAC.vue'
import EtcEgrMoter from './components/products/EtcEgrMoter.vue'
import LaserLevel from './components/products/LaserLevel.vue'
import AcCleaner from './components/products/AcCleaner.vue'
import BLDC from './components/products/BLDC.vue'
import FishingReel from './components/products/FishingReel.vue'
import Encoder from './components/products/Encoder.vue'
import OA from './components/products/OA.vue'
import PowerTool from './components/products/PowerTool.vue'
import RadiatorFanMotor from './components/products/RadiatorFanMotor.vue'
import EV_Traction_motor from './components/products/EV_Traction_motor.vue'
import Purge_Pump from './components/products/Purge_Pump.vue'
import Electric_Turbo_charger from './components/products/Electric_Turbo_charger.vue'
import Power_Lift_Gate from './components/products/Power_Lift_Gate.vue'
import Air_Cleaner_motor from './components/products/Air_Cleaner_motor.vue'
import Anemometer_Flowmeter from './components/products/Anemometer_Flowmeter.vue'
import Drone from './components/products/Drone.vue'


import Seal from './components/application/Seal.vue'
import Grease from './components/application/Grease.vue'
import Retainer from './components/application/Retainer.vue'
import Internal_Design from './components/application/Internal_Design.vue'
import Special_Design from './components/application/Special_Design.vue'

Vue.use(Router)
const testComponent = { template: '<div>foo</div>' }

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/application/',
      name: 'application',
      component: Product,
      children: [
        { path: 'Seal', component: Seal },
        { path: 'Grease', component: Grease },
        { path: 'Retainer', component: Retainer },
        { path: 'Internal_Design', component: Internal_Design },
        { path: 'Special_Design', component: Special_Design }
      ]
    },
    {
      path: '/product/',
      name: 'product',
      component: Product,
      children: [
        // { path: 'Turbo', component: Turbo },
        { path: "Purge_Pump", component: Purge_Pump},

        { path: "Electric_Turbo_charger", component: Electric_Turbo_charger},

        { path: "Power_Lift_Gate", component: Power_Lift_Gate},

        { path: "EV_Traction_motor", component: EV_Traction_motor},

        { path: "Air_Cleaner_motor", component: Air_Cleaner_motor},

        { path: "Anemometer_Flowmeter", component: Anemometer_Flowmeter},

        { path: "Drone", component: Drone},

        { path: 'ABS_ESC', component: ABS_ESC },
        { path: 'Wiper_motor', component: WiperMotor },
        { path: 'ETC_EGR_Value', component: EtcEgrValue },
        { path: 'Turbo', component: Turbo },
        { path: 'EPS_motor', component: EpsMotor },
        { path: 'ETC_EGR_motor', component: EtcEgrMoter },
        { path: 'unkonwn', component: testComponent },
        // 
        // {path: "unkonwn",component: testComponent},
        // {path: "unkonwn",component: testComponent},
        { path: 'Laser_leveler', component: LaserLevel },
        { path: 'Fishing_Reel', component: FishingReel },
        { path: 'Encoder', component: Encoder },
        { path: 'OA', component: OA },
        { path: 'Power_Tools', component: PowerTool },
        {
          path: 'Air_Cleaner_motor',
          component: testComponent
        },
        {
          path: 'AC_Cleaner_motor',
          component: AcCleaner
        },
        {
          path: 'BLDC_Cleaner_motor',
          component: BLDC
        },
        {
          path: 'HVAC_Blower_motor',
          component: HVAC
        },
        {
          path: 'Radiator_fan_motor',
          component: RadiatorFanMotor
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
