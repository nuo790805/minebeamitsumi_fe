const _APPs = {
  EN: {
    des: {
      grease: `Lubricant selection is very important to Ball bearing. It has a major influence on the ball bearing's Life, Loss torque, Rotation stability, Acoustic performance, Heat resistance, Cooling and anti-rust, etc.
      Minebea Group currently offers several hundred types of grease and oil, which caters to difference technical requirements such as Low-noise, Low torque, High temperature, Long life, etc.`,
      seal: `The purpose of shield/seal is to prevent contamination the enter the ball bearing interior, and grease leakage. According to different requirements. We have prepared various materials and designs to satisfy all needs.`,
      retainer: `Retainers keep the balls evenly spaced around the raceway, preventing ball to ball contact.We have prepared various materials and designs to satisfy all needs.`,
      inter: `Internal design refers to improve ball bearing performance by changing Curvature of bearing raceway, Dimension of P.C.D, Diameter and quantity of rolling element.
      It is done to satisfy the customer's special technical demands.`,
      special: `Special design means to change the external design of the bearing, change the bearing material, so as to greatly improve the performance of the bearing,
      and meet customer's special technical requirements.`
    },
    common: {
      bearing_type: 'Bearing type',
      standard: 'Standard',
      special: 'Special',
      Type: 'Type',
      non_contact_metal: 'Non contact metal',
      non_contact_rubber_seal: 'Non contact rubber seal',
      contact_rubber_seal: 'Contact rubber seal',
      light_contact: 'Light contact',
      heavy_contact: 'Heavy contact',
      two_point_contact: '2 point contact',
      reverse_contact: 'Reverse contact',
      Axial_motion_reinforced_version: 'Axial motion reinforced version',
      loss_torque: 'Loss torque',
      high_temperature_resistance: 'High temperature resistance',
      rubber_material: 'Rubber material',
      hardness_20_point_up: 'Hardness 20 point up',
      environmental_durability: 'Environmental durability',
      brake_oil: 'Brake oil',
      dimension_change: 'Dimension change',
      design: 'Design',
      standard_limit100: 'Assume the standard product is 100'
    }
  },
  CN: {
    des: {
      grease: `对于滚珠轴承，润滑油的选择是非常重要的。它在摩擦力距、旋转稳定性、噪声性能、耐高温性、冷却和防锈等方面对滚珠轴承的使用寿命有重大的影响。
      美蓓亚集团现有数百种油脂及油可供选择，可对应低噪音、低转矩、高温、高寿命等不同的技术要求。
      `,
      seal: `防尘盖的目的是防止异物侵入轴承内部和防止油脂的外渗。根据不同的要求，我们准备了不同的材料和设计，满足各位的要求。`,
      retainer: `保持架的作用是固定轴承内的滚珠，使其等间隔分布。根据不同的要求，我们准备了不同的材料和设计，满足各位的要求。`,
      inter: `内部设计的意思是，改变轴承滚道的曲率、改变滚珠的旋转半径、改变滚珠的尺寸和个数等，通过以上方法大幅提高轴承的性能，满足客户的特殊技术要求。`,
      special: `特殊设计的意思是，改变轴承的外部设计、改变轴承的材料，通过以上方法大幅提高轴承的性能，满足客户的特殊技术要求。`
    },
    common: {
      bearing_type: '轴承型号',
      standard: '标准',
      special: '特殊',
      Type: '种类',
      non_contact_metal: '非接触金属',
      non_contact_rubber_seal: '非接触橡胶盖',
      contact_rubber_seal: '接触橡胶盖',
      light_contact: '轻接触',
      heavy_contact: '重接触',
      two_point_contact: '2点接触',
      reverse_contact: '反向接触',
      Axial_motion_reinforced_version: '轴向移动加强型',
      loss_torque: '转距',
      high_temperature_resistance: '耐高温性',
      rubber_material: '橡胶材质',
      hardness_20_point_up: '硬度20point up',
      environmental_durability: '耐环境性',
      brake_oil: '刹车油',
      dimension_change: '尺寸变化',
      design: '形状',
      standard_limit100: '假设标准品为100'
    }
  },
  JP: {
    des: {
      grease: `ボールベアリングにとって、潤滑油の選定は非常に重要です。それは摩擦トルク、回転安定性、耐ノイズ性能、耐高温性、冷却と防錆などの面でボールベアリングの使用寿命に重大な影響を与えます。
      弊社では数百種のグリース及びオイルを取り揃えており、低ノイズ、低ロストルク、高温、長寿命など様々な技術要求に対応できます。`,
      seal: `シールの役割はベアリング内部への異物侵入やグリースの滲出の防止,気密性能の確保​です。様々な要求に対する各種材料やデザインを用意しており、お客様のご要望にお答えします。`,
      retainer: `リテーナの役割は、ベアリング中のボールを同じ間隔で分布するように固定することです。様々な要求に対する各種材料やデザインを用意しており、お客様のご要望にお答えします。`,
      inter: `内部設計とは、軸受転走溝の曲率及びボールの回転半径・サイズ・数などを変えることで、これによってベアリングの性能を大幅に向上させ、お客様の特殊な技術要求にお答えします。`,
      special: `特殊設計とは、ベアリングの外部設計や材料を変えることで、これを通してベアリングの性能を大幅に向上させ、お客様の特殊な技術要求にお答えします。`
    },
    common: {
      bearing_type: 'Bearing type',
      standard: '標準',
      special: '特殊',
      Type: '種類',
      non_contact_metal: '非接触金属',
      non_contact_rubber_seal: '非接触ゴムシール',
      contact_rubber_seal: '接触ゴムシール',
      light_contact: '軽接触',
      heavy_contact: '重接触',
      two_point_contact: '2点接触',
      reverse_contact: '逆方向接触',
      Axial_motion_reinforced_version: '軸方向運動強化型',
      loss_torque: 'ロストルク',
      high_temperature_resistance: '耐高温性',
      rubber_material: 'ゴム材質',
      hardness_20_point_up: '硬度20point up',
      environmental_durability: '耐環境性',
      brake_oil: 'ブレーキオイル',
      dimension_change: '寸法変化',
      design: '形状',
      standard_limit100: '標準品を100とします'
    }
  }
}

let APPs = {
  des: {
    grease: ``,
    seal: ``,
    retainer: ``,
    inter: ``,
    special: ``
  },
  common: {
    bearing_type: '',
    standard: '',
    special: '',
    Type: '',
    non_contact_metal: '',
    non_contact_rubber_seal: '',
    contact_rubber_seal: '',
    light_contact: '',
    heavy_contact: '',
    two_point_contact: '',
    reverse_contact: '',
    Axial_motion_reinforced_version: '',
    loss_torque: '',
    high_temperature_resistance: '',
    rubber_material: '',
    hardness_20_point_up: '',
    environmental_durability: '',
    brake_oil: '',
    dimension_change: '',
    design: '',
    standard_limit100: ''
  }
}
export { _APPs, APPs }
