const _Anemometer_Flowmeter = {
  EN: {
    des: `Objective: Increase measurement accuracy,  High reliability`,
    grease_title: `Optimize Lubricant oil`,
    grease_intro_context: `Anemometer，Flowmeter need lubricant oil for measurement accuracy.
We recommend lubricant oil which is low loss torque under low temperature and low amount of evaporation under high temperature.`,
    special_title: `Adopt Special steel`,
    special_intro_context: `The expected lifetime of Anemometer，Flowmeter is very long, therefore we adopt special material and increase reliability.`,
    n0: 'Metallographic structure',
    n1: '24 hours after',
    n2: '168 hours after',

    sen_0: `Salt spray test result
Conditions:  Intermittent 5% solution salt spray`,
    sen_1: `Use torque meter
Conditions:  DDR-1340HHR;  Temperature: 3℃​`,
    red_0: 'more than 7 times'
  },
  CN: {
    des: `努力的宗旨：提高测量精度，高可靠性`,
    grease_title: `优化润滑油`,
    grease_intro_context: `风速计、流量计需要使用润滑油以保证测量精度。
我们推荐的润滑油，在低温下具有低旋转扭矩并且在高温下蒸发量低。`,
    special_title: `使用特殊钢材`,
    special_intro_context: `风速计、流量计要求寿命非常长，
因此我们采用特殊材料，提高可靠性。`,
    n0: '金相图',
    n1: '24小时后',
    n2: '168小时后',

    sen_0: `盐雾试验结果
测试条件：5%盐水间歇喷雾`,
    sen_1: `使用扭矩测量仪​
条件：DDR-1340HHR  温度：3℃​`,
    red_0: '7倍以上'
  },
  JP: {
    des: `うれしさ: 測定精度の向上，高い信頼性`,
    grease_title: `オイルの最適化`,
    grease_intro_context: `風速計や流量計が正確な数値を表示するには、潤滑にオイルが必要です。 低温時はロストルクが小さく、高温時は蒸発量の小さいオイルを推奨致します。`,
    special_title: `特殊鋼材の採用`,
    special_intro_context: `風速計や流量計に求められる寿命は非常に長いため、耐食性のある材料を用い、信頼性を向上させました。`,

    n0: '金属組織図',
    n1: '24時間経過',
    n2: '168時間経過',

    sen_0: `塩水噴霧試験結果
条件：5%塩水　間欠噴霧`,
    sen_1: `トルクドライバで測定​
条件:DDR-1340HHR  温度：3℃​`,
    red_0: '7倍以上'
  }
}

let Anemometer_Flowmeter = {
    des: ``,
    grease_title: ``,
    grease_intro_context: ``,
    special_title: ``,
    special_intro_context: ``,
    n0: '',
    n1: '',
    n2: '',
  
    sen_0: '',
    sen_1: '',
    red_0: '',
    red_1: ''
}
export { _Anemometer_Flowmeter, Anemometer_Flowmeter }
