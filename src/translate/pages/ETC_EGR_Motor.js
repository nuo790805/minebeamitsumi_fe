const _EtcEgrMoter = {
  EN: {
    des: `Objective: Maintain stable quality and high reliability under harsh environments`,
    grease_title: `Optimize Grease`,
    grease_intro_context: `Optimizing the base oil of Fluorine grease, Adopting Hybrid grease blent Urea with Fluorine can decrease Starting loss torque under low temperature and increase reliability under high temparature.`,
    sen_0: `Durability test result (high temperature)
Conditions: Rotation speed of 608: 3000 rpm
Preload: 39 N;  Temperature: 180°C`,
    sen_1: `Low-temperature 
Conditions: 608 at -40°C;  use torque meter`
  },
  CN: {
    des: `努力的宗旨：保证严酷环境下的性能稳定性和高可靠性`,
    grease_title: `优化油脂`,
    grease_intro_context: `优化氟素油脂的基油，采用尿素、氟素的混合油脂可以在低温下减少启动扭矩的损失。`,
    sen_0: `耐久实验结果（高温）
条件：608  转速3000rpm  预压39N  温度180°C`,
    sen_1: `低温起动实验
条件：608  温度-40°C 使用转矩测量仪`
  },
  JP: {
    des: `うれしさ:  過酷環境下での安定した性能，高い信頼性の確保`,
    grease_title: `グリースの最適化​`,
    grease_intro_context: `基油成分を工夫したフッ素グリース，ウレアとフッ素を調合したハイブリッドグリースを採用し、低温での起動性と高温環境下での信頼性を両立させました。`,
    sen_0: `耐久試験結果（高温）
条件：608    回転数 3,000rpm.    予圧 39N    温度180°C`,
    sen_1: `低温起動試験
条件：608    温度 -40℃    トルクドライバー使用`
  }
}

let EtcEgrMoter = {
  des: ``,
  grease_title: ``,
  grease_intro_context: ``,
  sen_0: ``,
  sen_1: ``
}
export { _EtcEgrMoter, EtcEgrMoter }
