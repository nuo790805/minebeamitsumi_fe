const _ABS_ESC = {
  EN: {
    des: 'Delights: Simplify the manufacturing process and Increase reliability',
    seal_title: 'Optimize Rubber seal material',
    seal_intro_context:
      'Adopting EPDM Rubber seal can prevent expansion of it with Brake oil.',
    special_title:
      'Adopt Eccentric ball bearing',
    special_intro_context:
      'Pumps are subject to repeated and powerful impacts during operation. Needle roller or eccentric shaft are often adopted in the past. Eccentric Ball bearings that adopt eccentric inner ring designs can increase reliability and simplify the manufacturing process.',
    Eccentricity: 'Eccentricity',
    sen_0: `Expansion of rubber
Condition: Apply grease on the surface of the EPDM rubber and place in high temperature environment`,
    graph: `Reduced to 1 / 37`
  },
  CN: {
    des: '努力的宗旨：精简制造工序，提高可靠性',
    seal_title: '优化橡胶防尘盖的材料',
    seal_intro_context: '采用EPDM橡胶材料的防尘盖，可有效防止刹车油导致的橡胶膨胀。',
    special_title: '采用偏心轴承',
    special_intro_context:
      '泵体在运转时会反复受到强大冲击，过去通常会采用滚针轴承或采用偏心轴与球轴承。使用内径偏心设计的偏心轴承，能够提高可靠性，简化制造工序。​',
    Eccentricity: '偏心量',
    sen_0: `橡胶防尘盖的膨胀量
条件：EPDM橡胶表面涂抹油脂，
放置高温环境`,
    graph: `减至1 / 37`
  },
  JP: {
    des: 'うれしさ:  作業工程の単純化、信頼性の向上',
    seal_title: 'ゴムシール材料の最適化',
    seal_intro_context:
      'ブレーキオイル（グリコール系）に対し、ゴムシールが膨潤することのないよう、EPDM製ゴムシールを採用しています。',
    special_title:
      '偏芯ボールベアリングの採用',
    special_intro_context:
      '強い衝撃力を繰り返し受けるポンプユニット部には、これまでニードルベアリングや偏芯シャフトを用いたボールベアリングが採用されてきましたが、内径を偏芯させた偏芯ボールベアリングをご使用いただくと、作業工数の削減と信頼性の向上を両立することができます。',
    Eccentricity: '偏芯量',
    sen_0: `ゴムシールの膨潤量
条件：EPDM ゴムにブレーキオイル​を
塗布し、高温環境で放置`,
    graph: `1/37 に減少`
  }
}

let ABS_ESC = {
  des: '',
  seal_title: '',
  seal_intro_context: '',
  special_title: '',
  special_intro_context: '',
  Eccentricity: '',
  sen_0: ``,
  graph: ``
}

export { _ABS_ESC, ABS_ESC }
