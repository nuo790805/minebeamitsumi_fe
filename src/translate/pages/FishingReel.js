const _FishingReel = {
  EN: {
    des: `Delights: Maintenance-free`,
    special_title: `Adopt Special steel`,
    special_intro_context: `We developed Special Stainless steel for Bearings because Normal Stainless steel for Bearings (ex. SUS440C) rust under salt spray condition.
Optimize the structural components can increase both Antirust and Load capacity performance, therefore achieve longer operating life.`,
    grease_title: `Optimize Anti-rust oil`,
    grease_intro_context: `Optimizing Antirust oil can prevent the retention of seawater in Ball bearing. Therefore, decrease the rotational resistance by salt grains with keeping Antirust performance.`,
    n0: 'Metallographic structure',
    n1: '24 hours after',
    n2: '168 hours after',

    sen_0: `Salt spray test result
Conditions:  Intermittent 5% solution salt spray`,
    sen_1: `Confirmation of salt accumulation
Conditions: Weighed after 36 hours of DDL-850 seawater immersion`,
    red_0: 'more than 7 times',
    red_1: 'Reduced by 69%'
  },
  CN: {
    des: `努力的宗旨：免维护`,
    special_title: `使用特殊钢材`,
    special_intro_context: `因为普通轴承钢会生锈，美蓓亚开发了高防锈性特殊钢材。
能在满足抗负载与防锈能力的同时，实现长寿命。`,
    grease_title: `优化防锈油`,
    grease_intro_context: `优化防锈油可以防止滚珠轴承中的海水滞留。 从而在保证防锈性能的同时降低盐粒造成的旋转阻力。`,
    n0: '金相图',
    n1: '24小时后',
    n2: '168小时后',

    sen_0: `盐雾试验结果
测试条件：5%盐水间歇喷雾`,
    sen_1: `盐分堆积量确认结果
测试条件：DDL-850 海水浸泡放置
36小时后称重`,
    red_0: '7倍以上',
    red_1: '减少69%'
  },
  JP: {
    des: `うれしさ:  メンテナンスフリー`,
    special_title: `特殊鋼材の採用`,
    special_intro_context: `塩水噴霧条件では一般的な軸受用ステンレス鋼(SUS440C)は錆びてしまうため、構成成分の最適化により、耐荷重性と防錆能力を両立し、ロングライフを実現しています。`,
    grease_title: `防錆油の最適化`,
    grease_intro_context: `防錆油を工夫することで軸受内の海水滞留を抑制。防錆能力を維持しながら、塩噛み低減を実現しています。`,

    n0: '金属組織図',
    n1: '24時間経過',
    n2: '168時間経過',

    sen_0: `塩水噴霧試験結果
条件：5%塩水　間欠噴霧`,
    sen_1: `塩分堆積量確認結果
条件：DDL-850  海水浸漬36H放置　重量測定`,
    red_0: '7倍以上',
    red_1: '69%減'
  }
}

let FishingReel = {
  des: ``,
  special_title: ``,
  special_intro_context: ``,
  grease_title: ``,
  grease_intro_context: ``,
  n0: '',
  n1: '',
  n2: '',

  sen_0: '',
  sen_1: '',
  red_0: '',
  red_1: ''
}
export { _FishingReel, FishingReel }
