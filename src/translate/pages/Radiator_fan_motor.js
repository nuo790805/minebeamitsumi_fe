const _RadiatorFanMotor = {
  EN: {
    des: `Delights: Maintain high reliability under high temperature`,
    grease_title: `Optimize Grease`,
    grease_intro_context: `Adopting Hybrid grease blent Urea with Fluorine which have good performance under high temperature can increase reliability.`,
    seal_title: `Optimize Rubber seal material`,
    seal_intro_context: `Adopting Heat resistance rubber can increase reliability under high temparature.`,
    sen_0: `Durability test result (high temperature)
Conditions: Rotation speed of 608: 3000 rpm;   Preload: 39 N;   Temperature: 160°C
`,
    sen_1: `High temperature performance
test result of rubber seal
Conditions: The temperature when the hardness changes by 20 points after 1000 h`,
    red_0: `2 times`,
    red_1: `more than 4.8 times`
  },
  CN: {
    des: `努力的宗旨：保证高温环境下的可靠性`,
    grease_title: `优化油脂`,
    grease_intro_context: `采用尿素、氟素的混合油脂，在高温下保持良好性能并提高可靠性。`,
    seal_title: `优化橡胶防尘盖的材料`,
    seal_intro_context: `采用耐高温橡胶，提高在高温下的可靠性。`,
    sen_0: `耐久实验结果（高温）
条件：608  转速  3000rpm  预压39N  温度160°C`,
    sen_1: `橡胶材料防尘盖高温性能测试结果
条件：1000小时后硬度
产生20point变化时的温度`,
    red_0: `2倍`,
    red_1: `4.8倍以上`
  },
  JP: {
    des: `うれしさ:  高温環境下での高い信頼性の確保`,
    grease_title: `グリースの最適化`,
    grease_intro_context: `高温特性が良いウレアとフッ素を調合したハイブリッドグリースを採用し、高温環境下での信頼性を向上させました。`,
    seal_title: `ゴムシール材料の最適化`,
    seal_intro_context: `耐熱性を有するゴム材質を採用し、高温環境下での信頼性を向上させました。`,
    sen_0: `耐久試験結果（高温）
条件：608  回転数 3,000rpm.  予圧 39N  温度160°C`,
    sen_1: `ゴムシール耐熱性評価結果
条件：1000H後に 20point の硬度変化が
生じる温度を計測`,
    red_0: `2倍`,
    red_1: `4.8倍以上`
  }
}

let RadiatorFanMotor = {
  des: ``,
  grease_title: ``,
  grease_intro_context: ``,
  seal_title: ``,
  seal_intro_context: ``,
  sen_0: ``,
  sen_1: ``,
  red_0: ``,
  red_1: ``
}
export { _RadiatorFanMotor, RadiatorFanMotor }
