const _LaserLevel = {
  EN: {
    des: `Objective: Increase measurement accuracy`,
    retainer_title: `Optimize Retainer form`,
    retainer_intro_context: `Lightening Retainer and Optimizing Retainer form can decrease starting loss torque and increase measurement accuracy.`,
    sen_0: `Retainer weight (measured value)
Condition: R-830`,
    sen_1: `Retainer and steel balls clearance
(design median)
Condition: R-830`,
    sen_2: `Starting loss torque
Condition: R-830; Normal Temperature`,
    red_0: `Reduced by 73%`,
    red_1: `Reduced by 68%`,
    red_2: `twice`,
    red_3: `Reduced by 20%`,
    red_4: `Reduced by 40%`,
    term_0: 'Steel'
  },
  CN: {
    des: `努力的宗旨：提高测量精度`,
    retainer_title: `优化保持架的形状`,
    retainer_intro_context: `减轻保持架重量以及优化保持架形状可以减少起动扭矩的损失，并提高测量精度。`,
    sen_0: `保持架重量（实测值）
条件：R-830`,
    sen_1: `钢球与保持架之间的缝隙（设计中间值）
条件：R-830`,
    sen_2: `起动力矩
条件：R-830  常温`,
    red_0: `减少73%`,
    red_1: `减少68%`,
    red_2: `2倍`,
    red_3: `降低20%`,
    red_4: `降低40%`,
    term_0: '金属'
  },
  JP: {
    des: `うれしさ:  測定精度の向上`,
    retainer_title: `リテーナ形状の最適化`,
    retainer_intro_context: `リテーナの軽量化，リテーナボールポケットの形状変更により、起動時のロストルクを低減し、測定精度を向上しています。`,

    sen_0: `リテーナ重量（実測値）
条件：R-830`,
    sen_1: `ボール／リテーナのすきま（設計中央値）
条件：R-830`,
    sen_2: `起動時のロストルク
条件：R-830  常温`,
    red_0: `73%減`,
    red_1: `68%減`,
    red_2: `2倍`,
    red_3: `20%減`,
    red_4: `40%減`,
    term_0: '金属'
  }
}

let LaserLevel = {
  des: ``,
  retainer_title: ``,
  retainer_intro_context: ``,
  sen_0: ``,
  sen_1: ``,
  sen_2: ``,
  red_0: ``,
  red_1: ``,
  red_2: ``,
  red_3: ``,
  red_4: ``,
  term_0: ''
}
export { _LaserLevel, LaserLevel }
