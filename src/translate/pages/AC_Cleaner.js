const _AcCleaner = {
  EN: {
    des: `Delights: Increase motor efficiency`,
    inter_title: `Solution to achieve this property: Optimize internal design and retainer shape, reduce torque`,
    inter_intro_context: `Adjust Ball P.C.D. to reduce centrifugal force, lower contact pressure between the steel balls and the raceway, decrease loss torque`,
    inter2_title: ``,
    inter2_intro_context: `Optimize the shape design of the raceway, reduce the contact area between the steel balls and the raceway, decrease loss torque`,
    sen_0: `Calculated centrifugal force
Condition: 608`,
    sen_1: `Torque measurement result
Condition: Speed of 608: 45,000 RPM
Air pressure: 25 kPa`
  },
  CN: {
    des: `努力的宗旨：提高电机效率`,
    inter_title: `优化内部设计及保持架形状，减小转矩`,
    inter_intro_context: `调整钢球节圆半径降低钢球离心力，减小钢球与滚道的接触压力，降低转矩。 `,
    inter2_title: ``,
    inter2_intro_context: `优化滚道形状设计，减小钢球与滚道接触面积，减低转矩。 `,
    sen_0: `离心力计算结果
    条件：608`,
    sen_1: `转矩测量结果
条件：608 转速45,000RPM.
气压25KPA`
  },
  JP: {
    des: `うれしさ:  モータ効率の向上`,
    inter_title: `うれしさを実現する為の Solution：内部設計やリテーナ形状の最適化による回転時のロストルク低減`,
    inter_intro_context: `ボール回転半径の変更によりボールの遠心力が低減され、転走面との接触圧力が低減され、回転時のロストルクが低減されます`,
    inter2_title: ``,
    inter2_intro_context: `ボール転走溝形状の変更によってボールとリングの接触面積が小さくなり、回転時のロストルクが低減されます`,
    sen_0: `遠心力計算値
条件：608`,
    sen_1: `回転時のロストルク測定結果
条件：608 回転数 45,000rpm.
エア加圧 25kPa`
  }
}

let AcCleaner = {
  des: ``,
  inter_title: ``,
  inter_intro_context: ``,
  inter2_title: ``,
  inter2_intro_context: ``,
  sen_0: ``,
  sen_1: ``
}
export { _AcCleaner, AcCleaner }
