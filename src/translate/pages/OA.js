const _OA = {
  EN: {
    des: `Objective: Improve printing performance and diversify plastic product materials`,
    grease_title: `Solution to achieve this property: Optimize the performance of conductive grease`,
    grease_intro_context: `Select applicable base oil viscosity and adjust the ratio between thickeners and conductive materials for extended and stable conductivity`,
    grease2_title: `Solution to achieve this property: Adopt grease and anti - rust oil that prevent chemical attack on the plastic`,
    grease2_intro_context: `Prevent chemical attack while retaining life and anti - rust performance by adopting base oil that has a low chemical attack on the resin`,
    sen_0: `Conductivity test result
Test conditions: L-1260 rotation speed: 250
RPM; : 20N; Temperature: 50°C
Life:  For resistance (maximum) more than 100 kΩ`,
    sen_1: `Performance evaluation on resistance to chemical attack
Test conditions: 1/4 ellipse method; : room temperature; : 2 h
confirming crack locations`,
    sen_2: [
      'A.Recommended for conditions without abnormal deformation',
      'B.Can be normally used under normal conditions',
      'C.Not recommended(depending on the condition)',
      'The aforementioned data are based on plastic mainly used by the OA equipment maker and are not applicable for other general plastic'
    ]
  },
  CN: {
    des: `努力的宗旨：提高印刷性能，实现产品的树脂材料多元化`,
    grease_title: `导电性油脂的性能优化`,
    grease_intro_context: `选择适合使用条件的基油粘度，调整增稠剂和导电物质的配比实现长时间，稳定的导电性。`,
    grease2_title: `采用能抑制与树脂产生化学侵蚀的油脂和防锈油`,
    grease2_intro_context: `采用对树脂化学侵蚀性小的基油在保持寿命和防锈能力等性能的同时，抑制化学侵蚀。`,
    sen_0: `导电性试验测试结果
测试条件：L-1260转速250RPM
Fr 20N 温度50°C
寿命值：以电阻（最大）超过100KΩ为寿命值
`,
    sen_1: `化学侵蚀性能评价结果
测试条件：1/4椭圆法 温度 常温 时间 2小时
通过确认裂缝位置进行判定`,
    sen_2: [
      'A 无异常变形的情况下推荐使用',
      'B 在一般情况下，可正常使用',
      'C 不推荐（视情使用）',
      '以上为OA设备厂家主要用树脂的相关数据并不适用于其他一般树脂'
    ]
  },
  JP: {
    des: `うれしさ:  印字性能の向上，樹脂材料の多用化`,
    grease_title: `うれしさを実現する為の Solution：導電性グリースの高性能化`,
    grease_intro_context: `使用条件に合わせた基油粘度の選択、増ちょう剤の配合、導電物質の配合により安定した導電性を長時間維持できるようになりました`,
    grease2_title: `うれしさを実現する為の Solution：樹脂とのケミカルアタックを抑制するグリース，防錆油の採用`,
    grease2_intro_context: `樹脂に対し、ケミカルアタック性の小さい基油成分を採用
性能（寿命，防錆能力）を維持しながら、ケミカルアタック性を抑えました`,
    sen_0: `導電性寿命測定結果
条件：L-1260，回転数 250rpm.
Fr 20N 温度50°C
寿命：導電抵抗値（最大）が 100kΩを超えた時を寿命とした`,
    sen_1: `ケミカルアタック性能評価結果
条件：1/4楕円法　温度 常温  時間 2時間
き裂位置を確認し、判定を行った`,
    sen_2: [
      'A 異常変形のない場合におすすめです',
      'B 一般的状況下で正常に使用できます',
      'C おすすめしません（状況に応じて使用します）',
      'OA機器メーカが使用する主な樹脂に関するデータですカテゴリーにある全ての樹脂についての見解ではありません'
    ]
  }
}

let OA = {
  des: ``,
  grease_title: ``,
  grease_intro_context: ``,
  grease2_title: ``,
  grease2_intro_context: ``,
  sen_0: ``,
  sen_1: ``,
  sen_2: []
}
export { _OA, OA }
