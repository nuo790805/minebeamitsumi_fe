const _BLDC = {
  EN: {
    des: `Objective: Powerful suction and long life`,
    inter_title: `Optimize Retainer material`,
    inter_intro_context: `Adopting the good mechanical characteristic plastic (PEEK) can prevent Retainer deformation under high rotation speed, therefore decrease loss torque and achieve longer operating life.`,
    retainer2_title: ``,
    retainer2_intro_context: `Adopt plastic with improved mechanical performance (compared to the standard PA66+GF), suppress deformation induced by high-speed rotation, and extend the bearing lifespan.`,
    seal_title: `Optimize Rubber seal form`,
    seal_intro_context: `Optimizing Rubber seal design can increase dust-resistant performance with keeping loss torque level similar to Steel shield.`,
    sen_0: `Loss torque measurement result
Condition:  Speed of R-1560: 100,000 rpm`,
    sen_1: `Life test result
Condition: Speed of R-1560: 95,000 rpm`,
    sen_2: `Clearance between Iinner ring and rubber seal
Condition: Compared with the design median of R-1350`,
    red_0: `Reduced by 34%`,
    red_1: `more than 4.8 times`,
    red_2: `Reduced by 63%`,
    term_1: 'Non contact shield',
    term_2: 'Non contact rubber seal',
    term_3: 'Failure 250 hours after',
    term_4: 'Normal 1200 hours after'
  },
  CN: {
    des: `努力的宗旨：强吸力，延长马达寿命`,
    inter_title: `优化保持架的材料`,
    inter_intro_context: `采用具有良好机械特性的塑料（PEEK），可以防止保持架在高转速下的变形。从而降低旋转转矩，延长使用寿命。`,
    retainer2_title: ``,
    retainer2_intro_context: `采用比标准品(PA66+GF)机械性能更好的树脂材料、抑制高速旋转引起的变形，轴承寿命延长。`,
    seal_title: `优化橡胶防尘盖形状`,
    seal_intro_context: `优化橡胶防尘盖的设计，可以在提高防尘性能的同时，保持与金属防尘盖同级别的旋转扭矩。`,
    sen_0: `转矩测量结果
条件：R-1560 转速100,000rpm.`,
    sen_1: `寿命试验测试结果
条件：R-1560 转速95,000rpm.`,
    sen_2: `内轮和橡胶防尘盖的缝隙
条件：R-1350 设计中心值的对比`,
    red_0: `降低34%`,
    red_1: `4.8倍以上`,
    red_2: `减少63%`,
    term_1: '非接触金属',
    term_2: '非接触橡胶盖',
    term_3: '250小时后 失效',
    term_4: '1200小时后 无异常'
  },
  JP: {
    des: `うれしさ:  強い吸引力，モータ寿命の延長`,
    inter_title: `リテーナ材料の最適化`,
    inter_intro_context: `標準品（PA66+GF）より機械的特性の良い樹脂材料（PEEK）を用いることで、高速回転時の寸法変化を抑え、回転時のロストルク低減や長寿命を実現しています。`,
    retainer2_title: ``,
    retainer2_intro_context: `標準品（PA66+GF）より機械的特性の良い樹脂材料（PA9T，PEEK）を用いることで、高速回転時の寸法変化を抑え、回転時のロストルク低減を実現させています。`,
    seal_title: `ゴムシール形状の最適化`,
    seal_intro_context: `ゴムシール設計の最適化により、鉄製シールドのロストルクレベルを維持しながら、防塵性能を向上させています。`,
    sen_0: `回転時のロストルク測定結果
条件：R-1560 回転数 100,000rpm.`,
    sen_1: `寿命試験測定結果
条件：R-1560 回転数 95,000rpm.`,
    sen_2: `内輪とゴムシールのすきま量
条件：R-1350 設計中央値での比較`,
    red_0: `34%減`,
    red_1: `4.8倍以上`,
    red_2: `63%減`,
    term_1: '非接触金属',
    term_2: '非接触ゴムシール',
    term_3: '250時間経過　ロック',
    term_4: '1200時間経過　異常なし'
  }
}

let BLDC = {
  des: ``,
  retainer_title: ``,
  retainer_intro_context: ``,
  retainer2_title: ``,
  retainer2_intro_context: ``,
  seal_title: ``,
  seal_intro_context: ``,
  sen_0: ``,
  sen_1: ``,
  sen_2: ``,
  red_0: ``,
  red_1: ``,
  red_2: ``,
  term_1: '',
  term_2: '',
  term_3: '',
  term_4: ''
}
export { _BLDC, BLDC }
