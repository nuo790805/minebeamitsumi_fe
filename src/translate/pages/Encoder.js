const _Encoder = {
  EN: {
    des: `Delights: Increase detection accuracy`,
    seal_title: `Optimize Rubber seal form`,
    seal_intro_context: `Optimizing Rubber seal design can decrease the amount of grease scattering with keeping loss torque level similar to Steel shield.`,
    grease_title: `Optimize Grease`,
    grease_intro_context: `Adopting low-dispersion and low-evaporation grease`,
    n0: 'DD (contact rubber seal)',
    n1: 'DS (contact/non-contact rubber seal)',
    sen_0: `Loss torque measurement result
Condition: Rotation speed of
L-2112: 6000 rpm;  100°C`,
    sen_1: `Amount of grease splatter
Condition: Rotation speed of DDR-1350:
4000 rpm; 24 h; Temperature: 40°C;
Measure the number of particles per liter of air`
  },
  CN: {
    des: `努力的宗旨：提高测量精度`,
    seal_title: `优化橡胶防尘盖形状`,
    seal_intro_context: `优化橡胶防尘盖的设计，减少油脂渗出量，保持与金属防尘盖同级别的旋转扭矩。`,
    grease_title: `优化油脂`,
    grease_intro_context: `采用低飞散、低蒸发的油脂。`,
    n0: 'DD（两侧接触型）',
    n1: 'DS（接触/非接触型）',
    sen_1: `油脂飞散量
条件：DDR-1350  转速4000rpm.  24H  温度40°C测量每公升空气内的粒子数`,
    sen_0: `转矩测量结果
条件：L-2112  转速6000rpm.  100°C`
  },
  JP: {
    des: `うれしさ:  検出精度の向上`,
    seal_title: `ゴムシール形状の最適化`,
    seal_intro_context: `ゴムシール設計の最適化により、鉄製シールドのロストルクレベルを維持しながら、グリース飛散量を抑制しています。`,
    grease_title: `グリースの最適化`,
    grease_intro_context: `グリースが飛散，蒸発しにくいグリースを採用しています。`,
    n0: 'DD（両方接触型）',
    n1: 'DS（接触／非接触型）',
    sen_1: `グリース飛散量
条件：DDR-1350  回転数 4,000rpm.  24H  40℃体積1L 当たりの粒子数を測定`,
    sen_0: `回転時のロストルクデータ
条件：L-2112  回転数 6,000rpm. 100℃`
  }
}

let Encoder = {
  des: ``,
  seal_title: ``,
  seal_intro_context: ``,
  grease_title: ``,
  grease_intro_context: ``,

  n0: '',
  n1: '',
  sen_0: '',
  sen_1: ''
}
export { _Encoder, Encoder }
