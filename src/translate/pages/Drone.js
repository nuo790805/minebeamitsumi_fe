const _Drone = {
  EN: {
    des: `Objective: Reduce power consumption,  High reliability`,
    retainer_title: `Optimize Retainer form`,
    retainer_intro_context: `Battery-operated Drone have the limit of operating time, therefore we decrease loss torque for longer battery life.`,
    inter_title: `Optimize Raceway form`,
    inter_intro_context: ``,
    special_title: `Adopt Special steel`,
    special_intro_context: `Mainly Drone is used outside, exposed to rain and salt breeze, therefore we adopt special material and increase reliability.`,
    sen_0: `Salt spray test result
Conditions:  Intermittent 5% solution salt spray`,
    graph_0: 'Reduced by 25%',
    n0: 'Metallographic structure',
    n1: '24 hours after',
    n2: '168 hours after',
    red_0: 'more than 7 times'
  },
  CN: {
    des: `努力的宗旨：降低能耗、高可靠性
`,
    retainer_title: `优化保持架的形状`,
    retainer_intro_context: `使用电池包的无人机在运行时间上受限，因此我们通过降低旋转扭矩以增加电池的持久度。`,
    inter_title: `优化滚道的形状`,
    inter_intro_context: ``,
    special_title: `使用特殊钢材`,
    special_intro_context: `Drone主要在室外使用，因此会与雨水、湿气接触。使用耐腐蚀性材料，确保可靠性。`,
    sen_0: `盐雾试验结果
测试条件：5%盐水间歇喷雾`,
    graph_0: '25% 減',
    n0: '金相图',
    n1: '24小时后',
    n2: '168小时后',
    red_0: '7倍以上'
  },
  JP: {
    des: `うれしさ: 消費電力の低減，高い信頼性`,
    retainer_title: `リテーナ形状の最適化`,
    retainer_intro_context: `電池で駆動するタイプの Droneは、駆動時間に限りがあります。ボールベアリングのロストルクを下げることで、電池の"持ち"を良くしました。`,
    inter_title: `ボール転走溝形状の最適化`,
    inter_intro_context: ``,
    special_title: `特殊鋼材の採用`,
    special_intro_context: `Drone は主に屋外で使用され、雨に当ったり、潮風にさらされることもあるため、耐食性のある材料を用い、信頼性を向上させました。`,
    sen_0: `塩水噴霧試験結果
条件：5%塩水　間欠噴霧`,
    graph_0: '25%減',

    n0: '金属組織図',
    n1: '24時間経過',
    n2: '168時間経過',
    red_0: '7倍以上'
  }
}

let Drone = {
  des: ``,
  retainer_title: ``,
  retainer_intro_context: ``,
  inter_title: ``,
  inter_intro_context: ``,
  special_title: ``,
  special_intro_context: ``,
  sen_0:``,
  graph_0: '',
    n0: '',
    n1: '',
    n2: '',
    red_0: ''
}
export { _Drone, Drone }
