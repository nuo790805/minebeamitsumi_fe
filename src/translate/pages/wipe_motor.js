const _WiperMotor = {
  EN: {
    des: 'Delights: High wiping performance',
    inter_title:
      'Optimize Raceway form',
    inter_intro_context:
      'Deep groove Ball bearings are commonly used to bear the radial load. Optimizing Raceway form can make the axial load capacity increase.',
    touch: 'Contact Surface',
    increase: 'Increased by 212%',
    sen_0: `Calculated axial load capacity
Condition: 608`
  },
  CN: {
    des: '努力的宗旨：高刮拭性能',
    inter_title: '优化滚道的形状',
    inter_intro_context:
      '深沟球轴承通常用于承受径向负载，通过优化滚道形状大幅提高轴向承载力。',
    touch: '接触面',
    increase: '增加212 %',
    sen_0: `额定轴向负载（计算值）
条件：608`
  },
  JP: {
    des: 'うれしさ:  高いワイピング性能',
    inter_title:
      'ボール転走溝形状の最適化',
    inter_intro_context:
      '深溝玉軸受は主にラジアル荷重を受けるための軸受ですが、ボール転走溝の形状を最適化し、大きなアキシアル荷重を受けられるように致しました。',
    touch: '接触面',
    increase: '212%増',
    sen_0: `許容アキシアル荷重（計算値）
条件：608`
  }
}

let WiperMotor = {
  des: '',
  inter_title: '',
  inter_intro_context: '',
  touch: '',
  increase: '',
  sen_0: ''
}
export { _WiperMotor, WiperMotor }
