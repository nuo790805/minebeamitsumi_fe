const _Purge_Pump = {
  EN: {
    des: `Objective: Maintain stable quality and high reliability under harsh environments and high rotation speed`,
    red_1: `more than 4.8 times`,
    sen_1: `Life test result
Condition: Speed of R-1560: 95,000 rpm`,
    retainer_title: `Optimize Retainer material`,
    retainer_intro_context: `Adopting the good mechanical characteristic plastic (PEEK) can prevent Retainer deformation under high rotation speed and increase reliability under high temparature.`,
    special_title: `Optimize O-ring material, Calculate the fitting condition rigorously`,
    special_intro_context: `High rotation motor demand high assembly accuracy, therefore we propose Ball bearing with O-ring for keeping alignment.
And O-ring is exposed to gasoline, therefore we adopt the special material for it.
In addition, Calculating the fitting condition rigorously can give you the optimum dimension.`

  },
  CN: {
    des: `努力的宗旨：保证严酷环境下的性能稳定性和高可靠性`,
    red_1: `4.8倍以上`,
    sen_1: `寿命试验测试结果
条件：R-1560 转速95,000rpm.`,
    retainer_title: `优化保持架材料`,
    retainer_intro_context: `使用机械性非常好的树脂材料（PEEK)，抑制高速旋转引起的尺寸变化，提升高温环境下的可靠性。`,
    special_title: `优化O型圈材料，精确的配合计算`,
    special_intro_context: `高速旋转的电机需要高要求的装配精度。因此，我们建议使用带有O型圈的滚珠轴承来保持装配的一致。
并且，由于O型圈暴露在油气环境中，为此我们采用特殊材料来对应。
此外，严格计算配合条件，为您提供最佳尺寸。`
  },
  
  JP: {
    des: `うれしさ: 過酷環境，高速回転下での安定した性能，高い信頼性の確保`,
    red_1: `4.8倍以上`,
    sen_1: `寿命試験測定結果
条件：R-1560 回転数 95,000rpm.`,
    retainer_title: `リテーナ材料の最適化`,
    retainer_intro_context: `機械的特性の良い樹脂材料（PEEK)を用いることで、高速回転時のリテーナ変形を抑制。　また、高温環境での信頼性も向上させています。`,
    special_title: `O-ring 材料の最適化，厳密なはめあい計算`,
    special_intro_context: `高速回転のモータでは極めて高い組立精度が要求されるため、弊社では、同軸度の確保を目的として、O-ring 付きボールベアリングを提案しています。O-ring はガソリンと触れるため、特別な材料を選択しています。また、厳密なはめあい計算を行い、最適な寸法を提案致します。`
  }
  
}

let Purge_Pump = {
    des: ``,
    red_1:``,
    sen_1:``,
    retainer_title: ``,
    retainer_intro_context: ``,
    special_title: ``,
    special_intro_context: ``
}
export { _Purge_Pump, Purge_Pump }

