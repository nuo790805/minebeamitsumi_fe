const _Turbo = {
  EN: {
    des: 'Delights: Shorten response time',
    retainer_title:
      'Change to Ball bearing type',
    retainer_intro_context:
      'Replacing Oil retaining bearing (currently commonly applied in Turbo charger) with Ball bearing can shorten response time and realize smooth acceleration.',
    special_title:
      'Optimize retainer material',
    special_intro_context:
      'Adopt resin materials with improved mechanical performance (compared to the standard PA66+GF), prevent deformation induced by high-speed rotation to achieve high-speed operation, and extend life',
    sen_0: `Time to reach the specified rotation speed
Condition: Rotation speed: 120,000 rpm`,
    graph_0: 'Reduced by 25%',
    graph_1: '',
    graph_2: ''
  },
  CN: {
    des: '努力的宗旨：缩短反应时间',
    retainer_title:
      '改用滚珠轴承',
    retainer_intro_context:
      '目前为止涡轮增压主要使用含油轴承，改用滚珠轴承能使反应时间缩短，加速更流畅。',
    special_title:
      '优化保持架材料',
    special_intro_context:
      '采用标准品(PA66+GF)机械性能更好的树脂材料，抑制高速旋转引起的变形实现高速运转，延长寿命。',
    sen_0: `达到要求​转速的时间
条件：转速120,000rpm`,
    graph_0: '25% 減',
    graph_1: '',
    graph_2: ''
  },
  JP: {
    des: 'うれしさ:  応答時間の短縮',
    retainer_title:
      'ボールベアリングタイプへの変更',
    retainer_intro_context:
      'これまで滑り軸受が主流だったターボチャージャー用ベアリングにボールベアリングを採用いただくことで、応答時間の短縮、スムーズな加速が実現致しました。',
    special_title:
      'リテーナ材料の最適化',
    special_intro_context:
      '標準品(PA66+GF) より機械的特性の良い樹脂材料を用いることで、高速回転時の寸法変化を抑え、更なる高速回転化と寿命延長を実現させています',
    sen_0: `規定回転数までの立ち上がり時間
条件：回転数 120,000rpm.`,
    graph_0: '25%減',
    graph_1: '',
    graph_2: ''
  }
}

let Turbo = {
  des: '',
  retainer_title: '',
  special_title: '',
  special_intro_context: '',
  sen_0: '',
  graph_0: '',
  graph_1: '',
  graph_2: ''
}
export { _Turbo, Turbo }
