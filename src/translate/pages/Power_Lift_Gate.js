const _Power_Lift_Gate = {
  EN: {
    des: `Objective: Maintain high reliability under high loading and big impact`,
    inter_title: `Optimize Raceway form`,
    inter_intro_context: `Ball bearing for Power Back door have to bear high axial loading under operation and big axial impact in case of contact with the wall, the other car.
So we developed "Special Single row deepgroove ball bearing".`,
    sen_0:`Calculated axial load capacity
Condition: R-1760`,
    sen_1:`Axial clearance (measured value)
Condition: R-1760`,
    red_0:`1.8 times`,
    red_1:`Reduced by 74%`
  },
  CN: {
    des: `努力的宗旨：保证高负载，冲击环境下的高可靠性`,
    inter_title: `优化滚道的形状`,
    inter_intro_context: `电动后门的滚珠轴承在运行时必须承受较大的轴向负载，并且在与其他车辆、墙体发生撞击时也需要承受较大的轴向冲击。
因此，我们开发了“特殊单列深沟球轴承”。`,
    sen_0:`额定轴向负载（计算值）
条件：R-1760`,
    sen_1:`轴向游隙（实测值）
条件：R-1760`,
    red_0:`1.8倍`,
    red_1:`减少74%`
  },
  JP: {
    des: `うれしさ: 高荷重，衝撃環境下での高い信頼性の確保`,
    inter_title: `ボール転走溝形状の最適化`,
    inter_intro_context: `パワーバックドア用ボールベアリングは動作時、アキシアル方向に大きな負荷を受けます。　また、バックドアが壁や他の車と接触した際には、アキシアル方向に大きな衝撃を受けます。　それら荷重，衝撃に耐える単列深溝玉軸受を開発致しました。`,
    sen_0:`許容アキシアル荷重（計算値）
条件：R-1760`,
    sen_1:`アキシアルすきま（実測値）
条件：R-1760`,
    red_0:`1.8倍`,
    red_1:`74%减`
  }
}

let Power_Lift_Gate = {
    des: ``,
    inter_title: ``,
    inter_intro_context: ``,
    sen_0:``,
    sen_1:``,
    red_0:``,
    red_1:``
}
export { _Power_Lift_Gate, Power_Lift_Gate }
