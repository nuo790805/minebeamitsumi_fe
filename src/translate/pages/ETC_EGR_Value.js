const _EtcEgrValue = {
  EN: {
    des:
      'Objective: Maintain stable quality and high reliability under harsh environments',
    seal_title:
      'Optimize Rubber seal form',
    seal_intro_context:
      'Through Finite element analysis: Simulate rubber seal deformation under operation and optimize rubber seal form to meet performance requirements (ex. Low loss torque under low temperature and High airtigntness.)  ',
    seal2_title:
      'Optimize Rubber seal form',
    seal2_intro_context:
      'EGR Valve is directly exposed to exhaust gas, therefore we recommend using the heat-resistant rubber seal.',
    zero_press: 'No load',
    positive_press: 'Positive pressure: 300 kPa',
    sen_0: `Rubber seal heat resistance test result
Condition: Measure the temperature when the hardness increased to 20 points after 1000 hrs`,
    R2210: 'R-2210', 
    L1910: 'L-1910',
    red: `Increased 25%
Increased more than 67%`,
    red_0:'Increased 25%',
    red_1:`Increased 67%`,
    condition:''
  },
  CN: {
    des: '努力的宗旨：保证严酷环境下的性能稳定性和高可靠性',
    seal_title: '优化橡胶防尘盖的形状',
    seal_intro_context:
      '通过有限元分析：模拟橡胶密封件在运行中的变形，优化橡胶密封形式，满足性能要求（例如，低温下的低损耗扭矩和高气密性。）',
    seal2_title: '优化橡胶防尘盖的材料',
    seal2_intro_context:
      'EGR Valve直接曝露在废气环境中，因此必须考虑耐热性。美蓓亚根据耐热性要求推荐合适的橡胶防尘盖。',
    zero_press: '无负载',
    positive_press: '正压300kPa',
    sen_0: `橡胶防尘盖耐热性测试结果
条件：测量1000小时后，硬度上升20point时的温度`,
    R2210: 'R-2210',
    L1910: 'L-1910',
    red: '增加25%   增加多于67%',
    red_0:'增加25%',
    red_1:'增加多于67%',
    
    condition:''
  },
  JP: {
    des: 'うれしさ:  過酷環境下での安定した性能，高い信頼性の確保',
    seal_title: 'ゴムシール形状の最適化',
    seal_intro_context:
      '有限要素法を用い、動作時にゴムシールがどのように変形するかを計算。　低温での起動性と高い気密性を両立する設計等、要求性能に合うゴムシール形状を設計致します。​',
    seal2_title:
      'ゴムシール材料の最適化',
    seal2_intro_context:
      `EGR Valve は排気ガスに曝されるため、耐熱性を考慮しなければなりません。
耐熱性を考慮したゴムシールを提案致します。`,
    zero_press: '荷重無し',
    positive_press: '正圧 300kPa',
    sen_0:`ゴムシール耐熱性
条件：1000H後に 20point の
硬度変化が生じる温度を計測`,
    sen_0_0: `ゴムシール耐熱性`,
    sen_0_1: `1000H後に 20point の
硬度変化が生じる温度を計測`,
    R2210: 'R-2210',
    L1910: ' L-1910',
    red: '25%増   67%超増',
    red_0:'25%増',
    red_1:'67%超増',
    
    condition:'条件：'
  }
}

let EtcEgrValue = {
  des: '',
  seal_title: '',
  seal_intro_context: '',
  seal2_title: '',
  seal2_intro_context: '',
  zero_press: '',
  positive_press: '',
  NBR: '',
  HNBR: '',
  FKM: '',
  sen_0: ``,
  sen_0_0: ``,
  sen_0_1: ``,
  R2210: '',
  L1910: '',
  red: '',
  condition:''
}
export { _EtcEgrValue, EtcEgrValue }
