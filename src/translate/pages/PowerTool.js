const _PowerTool = {
  EN: {
    des: `Objective: Extend the product life,  Reduce power consumption`,
    seal_title: `Optimize Rubber seal form`,
    seal_intro_context: `Dust-resistant reqiurements vary by each Power tools application.
Therefore, we have many Rubber seal design and optimize the balance between Power consumption and Dust-resistant performance.`,
    retainer_title: `Optimize Retainer form`,
    retainer_intro_context: `Battery-operated Power tool have the limit of operating time, therefore we decrease loss torque for longer battery life.`,
    inter_title: `Optimize Raceway form`,
    inter_intro_context: ``,
    sen_0:`Loss torque measurement result
Conditions:
Rotation speed of R-1350: 2000 rpm;  Preload: 7.8 N;
Temperature: Normal Tempreature`,
    graph_0: 'Reduced by 31%'
  },
  CN: {
    des: `努力的宗旨：延长产品寿命，降低能耗
`,
    seal_title: `优化橡胶防尘盖形状`,
    seal_intro_context: `不同用途的电动工具对防尘性要求不同。美蓓亚针对各种用途，提供不同的橡胶防尘盖，优化能耗和防尘性的平衡。`,
    retainer_title: `优化保持架的形状`,
    retainer_intro_context: `使用电池包的电动工具在运行时间上受限，因此我们通过降低旋转扭矩以增加电池的持久度。`,
    inter_title: `优化滚道的形状`,
    inter_intro_context: ``,
    sen_0:`转矩测量结果
测试条件：R-1350  转速2000rpm 
预压7.8N  常温`,
    graph_0: '31% 減'
  },
  JP: {
    des: `うれしさ:  製品寿命の延長，消費電力の低減`,
    seal_title: `ゴムシール形状の最適化`,
    seal_intro_context: `用途によって求められる防塵性能が大きく異なるため、多くのゴムシール仕様を持つことで、消費電力と防塵性のバランスを最適化しています。`,
    retainer_title: `リテーナ形状の最適化`,
    retainer_intro_context: `電池で駆動するタイプの電動工具は、駆動時間に限りがあります。ボールベアリングのロストルクを下げることで、電池の"持ち"を良くしました。`,
    inter_title: `ボール転走溝形状の最適化`,
    inter_intro_context: ``,
    sen_0:`回転時のロストルク測定結果
条件:R-1350 回転数：2000rpm.
予圧：7.8N  温度：常温​`,
    graph_0: '31%減'

  }
}

let PowerTool = {
  des: ``,
  seal_title: ``,
  seal_intro_context: ``,
  retainer_title: ``,
  retainer_intro_context: ``,
  inter_title: ``,
  inter_intro_context: ``,
  sen_0:``,
  graph_0: ''
}
export { _PowerTool, PowerTool }
