const _EV_Traction_motor = {
  EN: {
    des: `Objective: Reduce the risk of electromagnetic noise and electric corrosion`,
    grease_title: `Optimize Conductive grease`,
    grease_intro_context: `EV Traction motor has the risk of developing electromagnetic noise and electric corrosion so we propose Ball bearing with high conductive performance under high rotation speed.`,
    seal_title: `Optimize Conductive Rubber seal`,
    seal_intro_context: ``,
    sen_0:`Conductivity test result
Condition：Rotation speed of L-1680: 2500rpm;
Temperature: Normal Tempreature 
Judgement: Insulation condition`,
    sen_1:`Volume resistance`,
    red_0:`3.8 times`,
    red_1:`Reduced to 1/10<sup>8</sup>`
  },
  CN: {
    des: `努力的宗旨：降低发生电磁噪音和电腐蚀的风险
`,
    grease_title: `采用导电性油脂`,
    grease_intro_context: `EV Traction motor是以高压、大电流驱动，因此轴承存在发生电腐蚀的风险。因为轴上带电会有发生电磁噪音的风险，推荐在高速旋转状态下也能长时间稳定运行的导电轴承。`,
    seal_title: `采用导电性橡胶防尘盖`,
    seal_intro_context: ``,
    sen_0:`导电性试验测试
条件：L-1680 转速2500rpm. 径向负载49N
温度：常温  判定：失去导电性能`,
    sen_1:`体积电阻率`,
    red_0:`3.8倍`,
    red_1:`减至 1/10<sup>8</sup>`
  },
  JP: {
    des: `うれしさ: 電磁ノイズリスク，電食発生リスクの低減`,
    grease_title: `導電性グリースの採用`,
    grease_intro_context: `EV Traction motor は高圧，大電流で駆動するため、ボールベアリングに電食が発生するリスク，シャフトに帯電することで電磁ノイズが発生するリスクがあります。高速回転でも長時間安定してアースできるボールベアリングを提案致します。`,
    seal_title: `導電性ゴムシールの採用`,
    seal_intro_context: ``,
    sen_0:`導電性寿命測定
条件：L-1680 回転数2500rpm.
ラジアル荷重49N
温度：常温  判定：絶縁状態`,
    sen_1:`体積抵抗率`,
    red_0:`3.8倍`,
    red_1:`1/10<sup>8</sup> に減少`
  }
}

let EV_Traction_motor = {
    des: ``,
    grease_title: ``,
    grease_intro_context: ``,
    seal_title: ``,
    seal_intro_context: ``,
    sen_0:``,
    sen_1:``,
    red_0:``,
    red_1:``
}
export { _EV_Traction_motor, EV_Traction_motor }
