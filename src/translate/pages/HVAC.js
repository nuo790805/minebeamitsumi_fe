const _HVAC = {
  EN: {
    des: `Delights: Increase accoustic performance`,
    retainer_title: `Optimize Retainer form`,
    retainer_intro_context: `Optimizing Retainer ball-pocket design can keep grease fluidity under low temperature and prevent noise.`,
    inter_title: `Optimize Raceway form`,
    inter_intro_context: `Optimizing Raceway form can decrease Ball play and prevent noise.`,
    sen_0: `Retainer and steel balls clearance (design value)
Condition: -40°C`,
    sen_1: `Angular clearance (design value)
Condition: When radial play is
M5 (13–20 µm)`,
    d30: 'Reduced by 30%',
    d90: 'Reduced by 90%',
    combine_text:'Noise in -40°C environment;  Rotation speed: 4200 RPM;  Fa: 70 N'
  },
  CN: {
    des: `努力的宗旨：静音性能`,
    retainer_title: `优化保持架的形状`,
    retainer_intro_context: `优化保持架滚珠槽设计，确保低温下油脂的流动性，降低噪音。`,
    inter_title: `优化滚道的形状`,
    inter_intro_context: `优化滚道降低钢球跳动并防止噪音发生。`,
    sen_0: `保持架和钢球的缝隙（设计值）
条件：-40°C`,
    sen_1: `角游隙量（设计中间​值）
条件：径向游隙M5(13-20um)时`,
    d30: '减少30%',
    d90: '减少90%',
    combine_text:'-40°C环境下的噪音  转速4200rpm.  预压70N'
  },
  JP: {
    des: `うれしさ:  静寂性`,
    retainer_title: `リテーナ形状の最適化`,
    retainer_intro_context: `リテーナボールポケットの形状を最適化することで、低温時のグリースの流動を確保し、異音発生を軽減致しました。`,
    inter_title: `ボール転走溝形状の最適化`,
    inter_intro_context: `ボール転走溝の形状を最適化することで、ボールの自由度を抑え、異音発生を軽減致しました。`,
    sen_0: `リテーナとボールのすきま（設計中央値）
条件：-40℃環境下`,
    sen_1: `角スキマ量（設計中央値）
条件：ラジアルすきま M5(13-20um) 時`,
    d30: '30%減',
    d90: '90%減',
    combine_text:'-40℃環境下ノイズ  回転数4200rpm.,  Fa=70N​'
  }
}

let HVAC = {
  des: ``,
  retainer_title: ``,
  retainer_intro_context: ``,
  inter_title: ``,
  inter_intro_context: ``,
  sen_0: ``,
  sen_1: ``,
  d30: '',
  d90: '',
  combine_text:''
}
export { _HVAC, HVAC }
