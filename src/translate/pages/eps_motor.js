const _EpsMotor = {
  EN: {
    des: 'Delights: Suppress vibration and Reduce noise',
    special_title:
      'Adopt Ball Bearing equipped with O-ring',
    special_intro_context:
      'Common problems of EPS motor consist of noise and vibration. Various Ball bearing with O-ring can reduce noise and control vibration.',
    special2_title:
      'Calculate the fitting condition rigorously',
    special2_intro_context:
      `Problems of EPS motor have its root in not only bearing inside scratche and contamination but also poor assembly.
Through calculating the fitting condition rigorously with material property and temperature condition we can provide the optimum the dimensions of parts.`
  },
  CN: {
    des: '努力的宗旨：抑制振动，降低噪音',
    special_title: '采用带O型圈的滚珠轴承',
    special_intro_context:
      'EPS电机的常见问题主要有异音及振动。美蓓亚开发的多种O型圈滚珠轴承，可有效降低噪音，抑制振动。',
    special2_title: '精密的配合计算',
    special2_intro_context:
      `EPS电机发生不良的原因，不仅有轴承内部伤痕以及异物，也有组装不良。
美蓓亚考虑材料特性、温度条件等因素，通过精密的配合计算，为客户提供最佳的配合尺寸。`
  },
  JP: {
    des: 'うれしさ:  振動抑制、ノイズの低減',
    special_title:
      'O-ring 付きボールベアリングの採用',
    special_intro_context:
      'EPS motor の主な不具合として異音や振動が挙げられます。異音や振動の緩和，抑制のため、複数の O-ring type を取り揃えております。',
    special2_title: '厳密なはめあい計算',
    special2_intro_context: `EPS motor の不具合原因にはボールベアリング内のキズやゴミだけでなく、アッセンブリー不良もあります。
材料特性，温度条件等も考慮した厳密なはめあい計算により、適切な部品寸法を提示致します。`
  }
  
}

let EpsMotor = {
  des: '',
  special_title: '',
  special_intro_context: '',
  special2_title: '',
  special2_intro_context: ''
}
export { _EpsMotor, EpsMotor }
