const _Air_Cleaner_motor = {
  EN: {
    des: `Objective: Reduce power consumption`,
    retainer_title: `Optimize Retainer form`,
    retainer_intro_context: `Air cleaner demand lower power consumption for continuous run.
Optimizing Retainer & Raceway form can decrease starting loss torque and consumer lower amounts of power.`,
    inter_title: `Optimize Raceway form`,
    inter_intro_context: ``,
    sen_0:`Loss torque measurement result
Condition: Rotation speed of 608: 2000 rpm;  Preload: 30 N`,
    graph_0: 'Reduced by 32%'
  },  
  CN: {
    des: `努力的宗旨：降低能耗
`,
    retainer_title: `优化保持架的形状`,
    retainer_intro_context: `AIR CLEANER可能会全日运转，因此降低能耗是必不可少的。
通过优化保持架形状、滚道形状，降低球轴承的旋转扭矩从而为降低能耗做出贡献。`,
    inter_title: `优化钢球滚道的形状`,
    inter_intro_context: ``,
    sen_0:`转矩测量结果
条件：608  转速2000rpm.  预压30N`,
    graph_0: '32% 減'
  },
  JP: {
    des: `うれしさ: 消費電力の低減
`,
    retainer_title: `リテーナ形状の最適化`,
    retainer_intro_context:  `Air cleaner は１日中運転することもあり、消費電力の低減が不可欠です。リテーナ形状やボール転走溝の最適化により、ボールベアリングのロストルクを低減し、消費電力の低減に貢献します。`,
    inter_title: `ボール転走溝形状の最適化`,
    inter_intro_context: ``,
    sen_0:`回転時のロストルク測定結果
条件：608  回転数 2,000rpm.  予圧 25N`,
    graph_0: '32%減'
  }
}

let Air_Cleaner_motor = {
    des: ``,
    retainer_title: ``,
    retainer_intro_context: ``,
    inter_title: ``,
    inter_intro_context: ``,
    sen_0:``,
    graph_0: ''
}
export { _Air_Cleaner_motor, Air_Cleaner_motor }
