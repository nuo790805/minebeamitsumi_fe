import { _Turbo, Turbo } from '@/translate/pages/turbo.js'
import { _ABS_ESC, ABS_ESC } from '@/translate/pages/abs_esc.js'
import { _WiperMotor, WiperMotor } from '@/translate/pages/wipe_motor.js'
import { _EtcEgrValue, EtcEgrValue } from '@/translate/pages/ETC_EGR_Value.js'
import { _EpsMotor, EpsMotor } from '@/translate/pages/eps_motor.js'
import { _HVAC, HVAC } from '@/translate/pages/HVAC.js'
import { _EtcEgrMoter, EtcEgrMoter } from '@/translate/pages/ETC_EGR_Motor.js'
import { _LaserLevel, LaserLevel } from '@/translate/pages/LaserLevel.js'
import { _AcCleaner, AcCleaner } from '@/translate/pages/AC_Cleaner.js'
import { _BLDC, BLDC } from '@/translate/pages/BLDC.js'
import { _FishingReel, FishingReel } from '@/translate/pages/FishingReel.js'
import { _Encoder, Encoder } from '@/translate/pages/Encoder.js'
import { _OA, OA } from '@/translate/pages/OA.js'
import { _PowerTool, PowerTool } from '@/translate/pages/PowerTool.js'
import { _Purge_Pump, Purge_Pump } from '@/translate/pages/Purge_Pump.js'
import { _Electric_Turbo_charger, Electric_Turbo_charger } from '@/translate/pages/Electric_Turbo_charger.js'
import { _Power_Lift_Gate, Power_Lift_Gate } from '@/translate/pages/Power_Lift_Gate.js'
import { _EV_Traction_motor, EV_Traction_motor } from '@/translate/pages/EV_Traction_motor.js'
import { _Air_Cleaner_motor, Air_Cleaner_motor } from '@/translate/pages/Air_Cleaner_motor.js'
import { _Anemometer_Flowmeter, Anemometer_Flowmeter } from '@/translate/pages/Anemometer_Flowmeter.js'
import { _Drone, Drone } from '@/translate/pages/Drone.js'



import {
  _RadiatorFanMotor,
  RadiatorFanMotor
} from '@/translate/pages/Radiator_fan_motor.js'

const Page = {
  Turbo,
  _Turbo,

  _ABS_ESC,
  ABS_ESC,

  _WiperMotor,
  WiperMotor,

  _EtcEgrValue,
  EtcEgrValue,

  _EpsMotor,
  EpsMotor,

  _HVAC,
  HVAC,

  _RadiatorFanMotor,
  RadiatorFanMotor,

  _EtcEgrMoter,
  EtcEgrMoter,

  _LaserLevel,
  LaserLevel,

  _AcCleaner,
  AcCleaner,

  _BLDC,
  BLDC,

  _FishingReel,
  FishingReel,

  _Encoder,
  Encoder,

  _OA,
  OA,

  _PowerTool,
  PowerTool,

  _Purge_Pump, Purge_Pump, 
  _Electric_Turbo_charger, Electric_Turbo_charger, 
  _Power_Lift_Gate, Power_Lift_Gate, 
  _EV_Traction_motor, EV_Traction_motor, 
  _Air_Cleaner_motor, Air_Cleaner_motor, 
  _Anemometer_Flowmeter, Anemometer_Flowmeter, 
  _Drone, Drone 
}
export { Page }
