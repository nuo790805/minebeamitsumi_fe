const _Seal = {
  EN: {
    sen_0: 'Dimension change',
    sen_1: 'Assume the standard product is 100',
    sen_2: 'Optimize design<br>(Antidust and torque loss during rotation at the same time)',
    sen_3: 'ex. R-1350 for Ventilation fan motor & Vacuum cleaner motor (BLDC)',
    // rotate
    gbRRC: 'Gap between inner ring and rubber cover ',
    dustprotection: 'Dust protection performance',
    loss_tor: 'Loss Torque during rotation',

    // limit
    sen_4:
      'Optimize design<br>(Obtain rubber cover deformation during usage through finite element analysis)',
    sen_5: 'ex. L-1910 for ETC Value',
    deformation_amount: 'Deformation amount',
    term_0: 'Dust protection performance',
    term_1: 'Air leakage performance',
    non_contact_rubber_seal:'Non contact rubber seal',
    Axial_motion_reinforced_version:'Axial motion reinforced version'
  },
  CN: {
    sen_0: '此数据不是指使用极限温度',
    sen_1: '假设标准品为100',
    sen_2: '优化设计（同时实现防尘性和旋转时的扭矩损耗）',
    sen_3: 'EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
    // rotate
    gbRRC: '内圈和橡胶防尘盖的间隙',
    dustprotection: '防尘性',
    loss_tor: '旋转时的扭矩损耗',

    // limit
    sen_4: '优化设计（通过有限元分析：模拟使用时的橡胶盖变形）',
    sen_5: 'ex. L-1910 for ETC Value',
    deformation_amount: '形变量',
    term_0: '防尘性能',
    term_1: '漏气性能',
    non_contact_rubber_seal:'非接触橡胶盖',
    Axial_motion_reinforced_version:'轴向移动加强型'
  },
  JP: {
    sen_0: 'このデータは使用極限温度ではありません',
    sen_1: '標準品を100とします',
    sen_2: '設計の最適化（防塵性と回転時のロストルクを同時に実現）​',
    sen_3: 'EX.R-1350 for換気扇モータ& BLDC クリーナモータ',
    // rotate
    gbRRC: '内輪とゴムシールのすきま量',
    dustprotection: '防塵性',
    loss_tor: '回転時のロストルク',

    // limit
    sen_4: '設計の最適化（有限要素法による使用時のゴムシール変形解析）​',
    sen_5: 'ex. L-1910 for ETC Value',
    deformation_amount: '変形量',
    term_0: '防塵性',
    term_1: '空気漏れ性能',
    non_contact_rubber_seal:'非接触<br>ゴムシール',
    Axial_motion_reinforced_version:'軸方向運動<br>強化型'
  }
}

let Seal = {
  sen_0: '',
  sen_1: '',
  sen_2: '',
  sen_3: '',
  gbRRC: '',
  dustprotection: '',
  loss_tor: '',
  sen_4: '',
  sen_5: '',

  deformation_amount: '',
  term_0: '',
  term_1: '',
  non_contact_rubber_seal:'',
  Axial_motion_reinforced_version:''
}
export { _Seal, Seal }
