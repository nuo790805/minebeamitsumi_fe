const _unsort = {
  CN: {
    design: '形状',
    for_automobiles: '1)汽车用',
    point_contact: '2点接触',
    applications: '2、应用领域',
    a_reduction_of_loss_torque_during_rotation: '2) 减少旋转时的扭矩损耗',
    application: '"限定用途',
    environmental_durability: '3、耐环境性',
    optimum_design: '4、优化设计',
    acoustic_performance: '噪声性能',
    actual_test: '实物试验',
    air_leakage_performance: '漏气性能',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor_&_bldc,_vacuum_cleaner_motor':
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
    fan_motor: '风扇电机"',
    office_automation_equipment: '办公自动化设备"',
    vacuum_cleaner_motor: '吸尘器"',
    assume_the_standard_product_is_: '假设标准品为100"',
    axial_motion_reinforced_version: '轴向移动加强型',
    ball_centrifugal_force: '"滚珠离心力',
    '※_assume_the_standard_product_is_': '※假设标准品为100"',
    'ball_pcd_/_ball_diameter': '滚珠旋转半径/滚珠直径',
    base_oil: '基油',
    brake_oil: '刹车油',
    'carbon_black,_lithium_soap': '炭黑，锂石碱',
    'chemical_reaction_tests,_etc': '化学反应试验等',
    conductivity: '导电性',
    contact_rubber_seal: '接触橡胶盖',
    countermeasure_product_: '对策品1',
    crown_retainersteel: '冠形钢保持架',
    deformation_amount: '形变量',
    deformation_reduction_under_high_speed_by_changing_material:
      '因材质变化减少高速旋转时的变形',
    demo_video: '演示视频',
    dimension_change: '"尺寸变化',
    dust_protection_performance: '防尘性能',
    ester: '酯',
    ex__for_air_air_purifiers: 'EX.608 FOR空气净化器电机',
    exclusive_application: '"限定用途',
    'fan_motor,_encoder': '风扇电机，编码器"',
    experimental_condition: '实验条件',
    fluorinated_grease: '氟素油脂',
    fluorine: '氟素',
    'fluorine_+_ester_+_synthetic_hydro_carbon': '氟素+酯+合成碳氢',
    'fluorine,_ester': '氟素，酯',
    'fluorine,_ester,_synthetic_hydro_carbon': '氟素，酯，合成碳氢',
    four_ball_test: '四球试验',
    gap_between_inner_ring_and_rubber_cover: '"内圈和橡胶防尘盖的间隙',
    grease_code: '油脂代号',
    heavy_contact: '重接触',
    high_rotation_speed: '高转速',
    high_speed: '高速性',
    high_temperature_resistance: '高温性',
    high_temperature_resistance_hardness__point_up: '"耐高温性(硬度20POINT UP)',
    this_data_does_not_refer_to_the_operating_temperature_limit:
      '此数据不是指使用极限温度"',
    high_versatility: '高泛用性',
    hybrid_grease: '混合型油脂',
    increase: '增加',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer,_and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '增加内圈、外圈和保持架间的距离、减少因油脂的剪切力对扭矩产生的影响',
    'inner_ring_curvature_/_outer_ring_curvature': '内圈曲率/外圈曲率',
    inner_ring_outer_diameter: '内圈外径',
    life: '寿命',
    life_test: '寿命试验',
    light_contact: '轻接触',
    lithium_soap: '锂石碱',
    lithium_soap_grease: '锂石碱油脂',
    long_life: '长寿命',
    'long_life,_high_rotation_speed': '长寿命 高转速',
    long_lifet_low_noise: '长寿命 低噪音',
    'long_lifetime,_low_dispersion': '长寿命 低起尘性',
    loss_torque: '转距',
    loss_torque_during_rotation: '旋转时的扭矩损耗',
    loss_torque_test: '扭矩损耗试验',
    low_temperature_resistance: '低温性',
    'low_temperature,_low_loss_torque': '低温 低扭矩损耗',
    material: '材质',
    multipurpose: '多用途',
    noise: '噪音',
    non_contact_metal: '非接触金属',
    non_contact_rubber_seal: '非接触橡胶盖',
    oil_film_thickness_test: '油膜厚度试验',
    oil_viscosity_: '基油粘度',
    operating_temperature: '"使用温度',
    changes_due_to_use_conditions: '根据使用条件有所变化"',
    other_evaluation: '其它评价',
    outer_ring_inner_diameter: '外圈内径',
    permissible_axial_loading_rp__mm: '"允许轴向负载(R.P. 0.020MM)',
    plastic_retainerpeek: 'PEEK树脂保持架',
    'plastic_retainerpolyamide_+_glass_fiber': '树脂保持架',
    properties: '功能性',
    ptfe: 'PTFE',
    'ptfe_+_urea': 'PTFE+尿素',
    'ptfe,_urea': 'PTFE,尿素',
    reduce: '减少',
    reverse_contact: '反向接触',
    ribbon_retainersteel: '波型钢保持架',
    rotation_speed: '回转速度',
    rubber_material: '橡胶材质',
    special: '特殊',
    standard: '标准',
    standard_product: '标准品',
    'standard/special': '标准/特殊',
    steel: '金属',
    temperature: '温度',
    'the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate_,_rpm':
      '利用有限元分析得出的，高速回转(100,000rpm.)时的保持架形状分析结果',
    the_gap_between_the_balls_and_the_bearing_retainer: '滚珠和保持架的间隙',
    theoretical_test: '理论试验',
    thickener: '增稠剂',
    type: '种类',
    urea: '尿素',
    urea_grease: '尿素油脂',
    worked_penetration: '混合稠度',
    worked_penetration_w: '混合稠度60W',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc,_vacuum_cleaner_motor':
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
    antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc__vacuum_cleaner_motor:
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
    carbon_black__lithium_soap: '炭黑，锂石碱',
    chemical_reaction_tests__etc: '化学反应试验等',
    fan_motor__encoder: '风扇电机，编码器"',
    fluorine__ester: '氟素，酯',
    fluorine__ester__synthetic_hydro_carbon: '氟素，酯，合成碳氢',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer__and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '增加内圈、外圈和保持架间的距离、减少因油脂的剪切力对扭矩产生的影响',
    long_life__high_rotation_speed: '长寿命 高转速',
    long_lifetime__low_dispersion: '长寿命 低起尘性',
    low_temperature__low_loss_torque: '低温 低扭矩损耗',
    ptfe__urea: 'PTFE,尿素',
    the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate___rpm:
      '利用有限元分析得出的，高速回转(100,000rpm.)时的保持架形状分析结果',
    fluorine___ester___synthetic_hydro_carbon: '氟素+酯+合成碳氢',
    plastic_retainerpolyamide___glass_fiber: '树脂保持架',
    ptfe___urea: 'PTFE+尿素',
    _assume_the_standard_product_is_: '※假设标准品为100"'
  },
  EN: {
    design: '1. Design',
    for_automobiles: '1) for Automobiles',
    point_contact: '2 point contact',
    applications: '2. Applications',
    a_reduction_of_loss_torque_during_rotation:
      '2) A reduction of loss torque during rotation',
    application: 'Application',
    environmental_durability: '3. Environmental durability',
    optimum_design: '4. Optimum design',
    acoustic_performance: 'Acoustic performance',
    actual_test: 'Actual test',
    air_leakage_performance: 'Air leakage performance',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor_&_bldc,_vacuum_cleaner_motor':
      '"防塵性と回転時のロストルクを同時に実現します EX. R-1350 FORファンモータ&BLDC クリーナモータ"',
    fan_motor: 'Fan motor',
    office_automation_equipment: 'Office automation equipment',
    vacuum_cleaner_motor: 'Vacuum cleaner motor',
    assume_the_standard_product_is_: 'Assume the standard product is 100',
    axial_motion_reinforced_version: 'Axial motion reinforced version',
    ball_centrifugal_force: 'Ball centrifugal force',
    '※_assume_the_standard_product_is_': '※ Assume the standard product is 100',
    'ball_pcd_/_ball_diameter': 'Ball P.C.D / Ball diameter',
    base_oil: 'Base oil',
    brake_oil: 'Brake oil',
    'carbon_black,_lithium_soap': 'カーボンブラック，リチウム石けん',
    'chemical_reaction_tests,_etc': 'ケミカルアタック試験等',
    conductivity: 'Conductivity',
    contact_rubber_seal: 'Contact rubber seal',
    countermeasure_product_: 'Countermeasure Product 1',
    crown_retainersteel: 'Crown Retainer(Steel)',
    deformation_amount: 'Deformation amount',
    deformation_reduction_under_high_speed_by_changing_material:
      'Deformation reduction under high speed by changing material',
    demo_video: 'Demo video',
    dimension_change: 'Dimension change',
    dust_protection_performance: 'Dust protection performance',
    ester: 'Ester',
    ex__for_air_air_purifiers: 'Ex. 608 for air Air purifiers',
    exclusive_application: 'Exclusive application',
    'fan_motor,_encoder': 'ファンモータ，エンコーダ"',
    experimental_condition: 'Experimental Condition',
    fluorinated_grease: 'Fluorinated grease',
    fluorine: 'Fluorine',
    'fluorine_+_ester_+_synthetic_hydro_carbon': 'フッ素+エステル+合成炭化水素',
    'fluorine,_ester': 'フッ素，エステル',
    'fluorine,_ester,_synthetic_hydro_carbon': 'フッ素，エステル，合成炭化水素',
    four_ball_test: 'Four ball test',
    gap_between_inner_ring_and_rubber_cover:
      'Gap between inner ring and rubber cover',
    grease_code: 'Grease code',
    heavy_contact: 'Heavy contact',
    high_rotation_speed: 'High rotation speed',
    high_speed: 'High speed',
    high_temperature_resistance: 'High temperature resistance',
    high_temperature_resistance_hardness__point_up:
      'High temperature resistance (Hardness 20 point up)',
    this_data_does_not_refer_to_the_operating_temperature_limit:
      'This data does not refer to the operating temperature limit',
    high_versatility: 'High versatility',
    hybrid_grease: 'Hybrid grease',
    increase: 'Increase',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer,_and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '内輪・外輪とリテーナとの距離を大きくし、グリースによるせん断力のトルクへの影響を低減します',
    'inner_ring_curvature_/_outer_ring_curvature':
      'Inner ring curvature / Outer ring curvature',
    inner_ring_outer_diameter: 'Inner ring outer diameter',
    life: 'Life',
    life_test: 'Life test',
    light_contact: 'Light contact',
    lithium_soap: 'Lithium soap',
    lithium_soap_grease: 'Lithium soap grease',
    long_life: 'Long life',
    'long_life,_high_rotation_speed': '長寿命 高速回転',
    long_lifet_low_noise: 'Long lifet Low noise',
    'long_lifetime,_low_dispersion': '長寿命 低発塵性',
    loss_torque: 'Loss torque',
    loss_torque_during_rotation: 'Loss torque during rotation',
    loss_torque_test: 'Loss torque test',
    low_temperature_resistance: 'Low temperature resistance',
    'low_temperature,_low_loss_torque': '低温 低ロストルク',
    material: 'Material',
    multipurpose: 'Multipurpose',
    noise: 'Noise',
    non_contact_metal: 'Non contact metal',
    non_contact_rubber_seal: 'Non contact rubber seal',
    oil_film_thickness_test: 'Oil film thickness test',
    oil_viscosity_: 'Oil viscosity ',
    operating_temperature: 'Operating temperature',
    changes_due_to_use_conditions: 'Changes due to use conditions',
    other_evaluation: 'Other evaluation',
    outer_ring_inner_diameter: 'Outer ring inner diameter',
    permissible_axial_loading_rp__mm:
      'Permissible axial loading (R.P. 0.020 MM)',
    plastic_retainerpeek: 'Plastic Retainer(PEEK)',
    'plastic_retainerpolyamide_+_glass_fiber': '樹脂リテーナ',
    properties: 'Properties',
    ptfe: 'PTFE',
    'ptfe_+_urea': 'PTFE+尿素',
    'ptfe,_urea': 'PTFE,尿素',
    reduce: 'Reduce',
    reverse_contact: 'Reverse contact',
    ribbon_retainersteel: 'Ribbon Retainer(Steel)',
    rotation_speed: 'Rotation speed',
    rubber_material: 'Rubber material',
    special: 'Special',
    standard: 'Standard',
    standard_product: 'Standard Product',
    'standard/special': 'Standard/Special',
    steel: 'Steel',
    temperature: 'Temperature',
    'the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate_,_rpm':
      '有限要素解析により取得したもの、高速回転(100,000rpm.)時のリテーナ形状の分析結果',
    the_gap_between_the_balls_and_the_bearing_retainer:
      'The gap between the balls and the bearing retainer',
    theoretical_test: 'Theoretical test',
    thickener: 'Thickener',
    type: 'Type',
    urea: 'Urea',
    urea_grease: 'Urea grease',
    worked_penetration: 'Worked Penetration',
    worked_penetration_w: 'Worked Penetration 60W',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc,_vacuum_cleaner_motor':
      '"防塵性と回転時のロストルクを同時に実現します EX. R-1350 FORファンモータ&BLDC クリーナモータ"',
    antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc__vacuum_cleaner_motor:
      'Antidust and torque loss during rotation at the same timeEx. R-1350 for Ventilation fan motor & BLDC, Vacuum cleaner motor',
    carbon_black__lithium_soap: 'Carbon black, Lithium soap',
    chemical_reaction_tests__etc: 'Chemical reaction tests, etc.',
    fan_motor__encoder: 'Fan motor, encoder',
    fluorine__ester: 'Fluorine, Ester',
    fluorine__ester__synthetic_hydro_carbon:
      'Fluorine, Ester, Synthetic hydro carbon',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer__and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      'Increase the distance between inner ring/outer ring and the bearing retainer, and reduce the influence on the loss torque by the shear force of the grease.',
    long_life__high_rotation_speed: 'Long life, High rotation speed',
    long_lifetime__low_dispersion: 'Long lifetime, Low dispersion',
    low_temperature__low_loss_torque: 'Low temperature, Low loss torque',
    ptfe__urea: 'PTFE, Urea',
    the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate___rpm:
      'The finite element analysis results for the bearing retainer deformation under a high rotation rate (100,000 rpm)',
    fluorine___ester___synthetic_hydro_carbon:
      'Fluorine + Ester + Synthetic hydro carbon',
    plastic_retainerpolyamide___glass_fiber:
      'Plastic Retainer(Polyamide + Glass fiber)',
    ptfe___urea: 'PTFE + Urea',
    _assume_the_standard_product_is_: '※標準品を100とします"'
  },
  JP: {
    design: '1、形状',
    for_automobiles: '1)自動車用途',
    point_contact: '2点接触',
    applications: '2、使用用途',
    a_reduction_of_loss_torque_during_rotation:
      '2) 回転時のロストルクを減らします',
    application: '"用途限定',
    environmental_durability: '3、耐環境性',
    optimum_design: '4、設計の最適化',
    acoustic_performance: '音響性能',
    actual_test: '実試験',
    air_leakage_performance: '空気漏れ性能',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor_&_bldc,_vacuum_cleaner_motor':
      '"防塵性と回転時のロストルクを同時に実現します EX. R-1350 FORファンモータ&BLDC クリーナモータ"',
    fan_motor: 'ファンモータ"',
    office_automation_equipment: 'OA"',
    vacuum_cleaner_motor: 'クリーナ"',
    assume_the_standard_product_is_: '標準品を100とします"',
    axial_motion_reinforced_version: '軸方向運動強化型',
    ball_centrifugal_force: '"ボール遠心力',
    '※_assume_the_standard_product_is_': '※標準品を100とします"',
    'ball_pcd_/_ball_diameter': 'ボール回転半径/ボール直径',
    base_oil: '基油',
    brake_oil: 'ブレーキオイル',
    'carbon_black,_lithium_soap': 'カーボンブラック，リチウム石けん',
    'chemical_reaction_tests,_etc': 'ケミカルアタック試験等',
    conductivity: '導電性',
    contact_rubber_seal: '接触ゴムシール',
    countermeasure_product_: '対策品①',
    crown_retainersteel: '鋼板冠型リテーナ',
    deformation_amount: '変形量',
    deformation_reduction_under_high_speed_by_changing_material:
      '材質の変化により高速回転時の変形を減らします',
    demo_video: 'デモ',
    dimension_change: '"寸法変化',
    dust_protection_performance: '防塵性',
    ester: 'エステル',
    ex__for_air_air_purifiers: 'EX.608 FOR空気清浄機用',
    exclusive_application: '"用途限定',
    'fan_motor,_encoder': 'ファンモータ，エンコーダ"',
    experimental_condition: '条件',
    fluorinated_grease: 'フッ素グリース',
    fluorine: 'フッ素',
    'fluorine_+_ester_+_synthetic_hydro_carbon': 'フッ素+エステル+合成炭化水素',
    'fluorine,_ester': 'フッ素，エステル',
    'fluorine,_ester,_synthetic_hydro_carbon': 'フッ素，エステル，合成炭化水素',
    four_ball_test: '四球試験',
    gap_between_inner_ring_and_rubber_cover: '"内輪とゴムシールのすきま量',
    grease_code: 'グリース番号',
    heavy_contact: '重接触',
    high_rotation_speed: '高速回転',
    high_speed: '高速性',
    high_temperature_resistance: '高温寿命',
    high_temperature_resistance_hardness__point_up: '"耐高温性(硬度20POINT UP)',
    this_data_does_not_refer_to_the_operating_temperature_limit:
      'このデータは使用極限温度ではありません"',
    high_versatility: '高い汎用性',
    hybrid_grease: 'ハイブリッドグリース',
    increase: '增加',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer,_and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '内輪・外輪とリテーナとの距離を大きくし、グリースによるせん断力のトルクへの影響を低減します',
    'inner_ring_curvature_/_outer_ring_curvature': '内輪曲率/外輪曲率',
    inner_ring_outer_diameter: '内輪外径',
    life: '寿命',
    life_test: '寿命試験',
    light_contact: '軽接触',
    lithium_soap: 'リチウム石けん',
    lithium_soap_grease: 'リチウム石けんグリース',
    long_life: '長寿命',
    'long_life,_high_rotation_speed': '長寿命 高速回転',
    long_lifet_low_noise: '長寿命 低騒音',
    'long_lifetime,_low_dispersion': '長寿命 低発塵性',
    loss_torque: 'ロストルク',
    loss_torque_during_rotation: '回転時のロストルク',
    loss_torque_test: 'ロストルク試験',
    low_temperature_resistance: '低温起動性',
    'low_temperature,_low_loss_torque': '低温 低ロストルク',
    material: '材質',
    multipurpose: '多用途',
    noise: '音響性能',
    non_contact_metal: '非接触金属',
    non_contact_rubber_seal: '非接触ゴムシール',
    oil_film_thickness_test: '油膜厚さ実測',
    oil_viscosity_: '基油粘度',
    operating_temperature: '"使用温度',
    changes_due_to_use_conditions: '使用条件により変化します"',
    other_evaluation: 'その他評価',
    outer_ring_inner_diameter: '外輪内径',
    permissible_axial_loading_rp__mm: '"許可アキシアル荷重(R.P. 0.020MM)',
    plastic_retainerpeek: 'PEEK樹脂リテーナ',
    'plastic_retainerpolyamide_+_glass_fiber': '樹脂リテーナ',
    properties: '機能性',
    ptfe: 'PTFE',
    'ptfe_+_urea': 'PTFE+ウレア',
    'ptfe,_urea': 'PTFE,ウレア',
    reduce: '減少',
    reverse_contact: '逆方向接触',
    ribbon_retainersteel: '鋼板波型リテーナ',
    rotation_speed: '回転数',
    rubber_material: 'ゴム材質',
    special: '特殊',
    standard: '標準',
    standard_product: '標準品',
    'standard/special': '標準/特殊',
    steel: '金属',
    temperature: '温度',
    'the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate_,_rpm':
      '有限要素解析により取得したもの、高速回転(100,000rpm.)時のリテーナ形状の分析結果',
    the_gap_between_the_balls_and_the_bearing_retainer:
      'ボールとリテーナのすきま',
    theoretical_test: '理論試験',
    thickener: '増ちょう剤',
    type: '種類',
    urea: 'ウレア',
    urea_grease: 'ウレアグリース',
    worked_penetration: '混和ちょう度',
    worked_penetration_w: '混和ちょう度60W',
    'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc,_vacuum_cleaner_motor':
      '"防塵性と回転時のロストルクを同時に実現します EX. R-1350 FORファンモータ&BLDC クリーナモータ"',
    antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc__vacuum_cleaner_motor:
      '"防塵性と回転時のロストルクを同時に実現します EX. R-1350 FORファンモータ&BLDC クリーナモータ"',
    carbon_black__lithium_soap: 'カーボンブラック，リチウム石けん',
    chemical_reaction_tests__etc: 'ケミカルアタック試験等',
    fan_motor__encoder: 'ファンモータ，エンコーダ"',
    fluorine__ester: 'フッ素，エステル',
    fluorine__ester__synthetic_hydro_carbon: 'フッ素，エステル，合成炭化水素',
    'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer__and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '内輪・外輪とリテーナとの距離を大きくし、グリースによるせん断力のトルクへの影響を低減します',
    long_life__high_rotation_speed: '長寿命 高速回転',
    long_lifetime__low_dispersion: '長寿命 低発塵性',
    low_temperature__low_loss_torque: '低温 低ロストルク',
    ptfe__urea: 'PTFE,ウレア',
    the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate___rpm:
      '有限要素解析により取得したもの、高速回転(100,000rpm.)時のリテーナ形状の分析結果',
    fluorine___ester___synthetic_hydro_carbon: 'フッ素+エステル+合成炭化水素',
    plastic_retainerpolyamide___glass_fiber: '樹脂リテーナ',
    ptfe___urea: 'PTFE+ウレア',
    _assume_the_standard_product_is_: '※標準品を100とします"'
  }
}

const unsort = {
  design: '形状',
  for_automobiles: '1)汽车用',
  point_contact: '2点接触',
  applications: '2、应用领域',
  a_reduction_of_loss_torque_during_rotation: '2) 减少旋转时的扭矩损耗',
  application: '"限定用途',
  environmental_durability: '3、耐环境性',
  optimum_design: '4、优化设计',
  acoustic_performance: '噪声性能',
  actual_test: '实物试验',
  air_leakage_performance: '漏气性能',
  'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor_&_bldc,_vacuum_cleaner_motor':
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
  fan_motor: '风扇电机"',
  office_automation_equipment: '办公自动化设备"',
  vacuum_cleaner_motor: '吸尘器"',
  assume_the_standard_product_is_: '假设标准品为100"',
  axial_motion_reinforced_version: '轴向移动加强型',
  ball_centrifugal_force: '"滚珠离心力',
  '※_assume_the_standard_product_is_': '※假设标准品为100"',
  'ball_pcd_/_ball_diameter': '滚珠旋转半径/滚珠直径',
  base_oil: '基油',
  brake_oil: '刹车油',
  'carbon_black,_lithium_soap': '炭黑，锂石碱',
  'chemical_reaction_tests,_etc': '化学反应试验等',
  conductivity: '导电性',
  contact_rubber_seal: '接触橡胶盖',
  countermeasure_product_: '对策品1',
  crown_retainersteel: '冠形钢保持架',
  deformation_amount: '形变量',
  deformation_reduction_under_high_speed_by_changing_material:
      '因材质变化减少高速旋转时的变形',
  demo_video: '演示视频',
  dimension_change: '"尺寸变化',
  dust_protection_performance: '防尘性能',
  ester: '酯',
  ex__for_air_air_purifiers: 'EX.608 FOR空气净化器电机',
  exclusive_application: '"限定用途',
  'fan_motor,_encoder': '风扇电机，编码器"',
  experimental_condition: '实验条件',
  fluorinated_grease: '氟素油脂',
  fluorine: '氟素',
  'fluorine_+_ester_+_synthetic_hydro_carbon': '氟素+酯+合成碳氢',
  'fluorine,_ester': '氟素，酯',
  'fluorine,_ester,_synthetic_hydro_carbon': '氟素，酯，合成碳氢',
  four_ball_test: '四球试验',
  gap_between_inner_ring_and_rubber_cover: '"内圈和橡胶防尘盖的间隙',
  grease_code: '油脂代号',
  heavy_contact: '重接触',
  high_rotation_speed: '高转速',
  high_speed: '高速性',
  high_temperature_resistance: '高温性',
  high_temperature_resistance_hardness__point_up: '"耐高温性(硬度20POINT UP)',
  this_data_does_not_refer_to_the_operating_temperature_limit:
      '此数据不是指使用极限温度"',
  high_versatility: '高泛用性',
  hybrid_grease: '混合型油脂',
  increase: '增加',
  'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer,_and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '增加内圈、外圈和保持架间的距离、减少因油脂的剪切力对扭矩产生的影响',
  'inner_ring_curvature_/_outer_ring_curvature': '内圈曲率/外圈曲率',
  inner_ring_outer_diameter: '内圈外径',
  life: '寿命',
  life_test: '寿命试验',
  light_contact: '轻接触',
  lithium_soap: '锂石碱',
  lithium_soap_grease: '锂石碱油脂',
  long_life: '长寿命',
  'long_life,_high_rotation_speed': '长寿命 高转速',
  long_lifet_low_noise: '长寿命 低噪音',
  'long_lifetime,_low_dispersion': '长寿命 低起尘性',
  loss_torque: '转距',
  loss_torque_during_rotation: '旋转时的扭矩损耗',
  loss_torque_test: '扭矩损耗试验',
  low_temperature_resistance: '低温性',
  'low_temperature,_low_loss_torque': '低温 低扭矩损耗',
  material: '材质',
  multipurpose: '多用途',
  noise: '噪音',
  non_contact_metal: '非接触金属',
  non_contact_rubber_seal: '非接触橡胶盖',
  oil_film_thickness_test: '油膜厚度试验',
  oil_viscosity_: '基油粘度',
  operating_temperature: '"使用温度',
  changes_due_to_use_conditions: '根据使用条件有所变化"',
  other_evaluation: '其它评价',
  outer_ring_inner_diameter: '外圈内径',
  permissible_axial_loading_rp__mm: '"允许轴向负载(R.P. 0.020MM)',
  plastic_retainerpeek: 'PEEK树脂保持架',
  'plastic_retainerpolyamide_+_glass_fiber': '树脂保持架',
  properties: '功能性',
  ptfe: 'PTFE',
  'ptfe_+_urea': 'PTFE+ウレア',
  'ptfe,_urea': 'PTFE,尿素',
  reduce: '减少',
  reverse_contact: '反向接触',
  ribbon_retainersteel: '波型钢保持架',
  rotation_speed: '回转速度',
  rubber_material: '橡胶材质',
  special: '特殊',
  standard: '标准',
  standard_product: '标准品',
  'standard/special': '标准/特殊',
  steel: '金属',
  temperature: '温度',
  'the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate_,_rpm':
      '利用有限元分析得出的，高速回转(100,000rpm.)时的保持架形状分析结果',
  the_gap_between_the_balls_and_the_bearing_retainer: '滚珠和保持架的间隙',
  theoretical_test: '理论试验',
  thickener: '增稠剂',
  type: '种类',
  urea: '尿素',
  urea_grease: '尿素油脂',
  worked_penetration: '混合稠度',
  worked_penetration_w: '混合稠度60W',
  'antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc,_vacuum_cleaner_motor':
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
  antidust_and_torque_loss_during_rotation_at_the_same_timeex_r__for_ventilation_fan_motor__bldc__vacuum_cleaner_motor:
      '同时实现防尘性和旋转时的扭矩损耗EX. R-1350 FOR换气扇电机&直流无刷 吸尘器电机',
  carbon_black__lithium_soap: '炭黑，锂石碱',
  chemical_reaction_tests__etc: '化学反应试验等',
  fan_motor__encoder: '风扇电机，编码器"',
  fluorine__ester: '氟素，酯',
  fluorine__ester__synthetic_hydro_carbon: '氟素，酯，合成碳氢',
  'increase_the_distance_between_inner_ring/outer_ring_and_the_bearing_retainer__and_reduce_the_influence_on_the_loss_torque_by_the_shear_force_of_the_grease':
      '增加内圈、外圈和保持架间的距离、减少因油脂的剪切力对扭矩产生的影响',
  long_life__high_rotation_speed: '长寿命 高转速',
  long_lifetime__low_dispersion: '长寿命 低起尘性',
  low_temperature__low_loss_torque: '低温 低扭矩损耗',
  ptfe__urea: 'PTFE,尿素',
  the_finite_element_analysis_results_for_the_bearing_retainer_deformation_under_a_high_rotation_rate___rpm:
      '利用有限元分析得出的，高速回转(100,000rpm.)时的保持架形状分析结果',
  fluorine___ester___synthetic_hydro_carbon: '氟素+酯+合成碳氢',
  plastic_retainerpolyamide___glass_fiber: '树脂保持架',
  ptfe___urea: 'PTFE+尿素',
  _assume_the_standard_product_is_: '※假设标准品为100"'
}

export { _unsort, unsort }
