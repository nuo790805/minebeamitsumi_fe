const _Inter = {
  EN: {
    sen_0: 'Optimize design<br>(Complete internal design for high speed rotation)',
    sen_1:
      'To increase motor efficiency, a ball bearing with low rotational loss torque. Ex. 608 for AC, with brush, vacuum cleaner motor (high efficiency)',
    sen_2: 'Optimize design<br>(Internal design for high durability under axial loading)',
    sen_3:
      'A ball bearing which supports high axial loading is required in order to wipe water during high speed driving ex. 608 for Air cleaner motor',
    gbRRC: '',
    dustprotection: '',
    loss_tor: '',
    sen_4: 'Inner ring outer diameter',
    sen_5: 'Outer ring inner diameter',
    sen_6: 'Permissible axial loading (R.P. 0.020 mm)',

    rf: 'Ball P.C.D / Ball diameter',
    n_o: 'Inner ring curvature / Outer ring curvature',
    ball_force: 'Ball centrifugal force',
    loss: 'Loss torque during rotation ',
    sub: 'Reduce',
    add: 'Increase',
    deformation_amount: ''
  },
  CN: {
    sen_0: '优化设计（实现高速旋转的内部设计）',
    sen_1: '为了提高马达效率，需要旋转扭矩损耗小的轴承',
    sen_2: '优化设计（耐高轴向负载的内部设计）',
    sen_3:
      '为了能在高速行驶时刮水，需要耐高轴向负载的轴承 ex. 608 for Air cleaner motor',
    gbRRC: '',
    dustprotection: '',
    loss_tor: '',
    sen_4: '内圈外径',
    sen_5: '外圈内径',
    sen_6: '允许轴向负载(R.P. 0.020mm)',

    rf: '滚珠旋转半径/滚珠直径',
    n_o: '内圈曲率/外圈曲率',
    ball_force: '滚珠离心力',
    loss: '旋转时的扭矩损耗',
    sub: '减少',
    add: '增加',
    deformation_amount: ''
  },
  JP: {
    sen_0: '設計の最適化（​高速回転を達成する内部設計）',
    sen_1:
      'モーター効率を向上させるには、回転ロストルクの小さいベアリングが必要です',
    sen_2: '設計の最適化（耐高アキシアル荷重の内部設計）​',
    sen_3:
      '高速走行時に水をはじくには、耐高アキシアル荷重のベアリングが必要です ex. 608 for Air cleaner motor',
    gbRRC: '',
    dustprotection: '',
    loss_tor: '',
    sen_4: '内輪外径',
    sen_5: '外輪内径',
    sen_6: '許可アキシアル荷重(R.P. 0.020mm)',
    rf: 'ボール回転半径/ボール直径',
    n_o: '内輪曲率/外輪曲率',
    ball_force: 'ボール遠心力',
    loss: '回転時のロストルク',
    sub: '減少',
    add: '增加',

    deformation_amount: ''
  }
}

let Inter = {
  sen_0: '',
  sen_1: '',
  sen_2: '',
  sen_3: '',
  gbRRC: '',
  dustprotection: '',
  loss_tor: '',
  sen_4: '',
  sen_5: '',
  rf: '',
  n_o: '',
  ball_force: '',
  loss: '',
  sub: '',
  add: '',

  deformation_amount: ''
}
export { _Inter, Inter }
