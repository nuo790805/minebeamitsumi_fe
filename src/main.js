import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import touch from 'vue-directive-touch'
import mixin from './mixin.js'

Vue.config.productionTip = true
Vue.config.devtools = true

Vue.use(touch)
Vue.mixin(mixin)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
