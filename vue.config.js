module.exports = {
  runtimeCompiler: true,
  publicPath: "",
  outputDir: undefined,
  assetsDir: undefined,
  productionSourceMap: undefined,
  parallel: true,
  css: undefined
  // filenameHashing: false
};
